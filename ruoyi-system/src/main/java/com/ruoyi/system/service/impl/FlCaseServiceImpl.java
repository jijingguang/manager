package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.domain.flz.FlCase;
import com.ruoyi.system.mapper.flz.FlCaseMapper;
import com.ruoyi.system.service.IFlCaseService;
import com.ruoyi.common.core.text.Convert;

/**
 * 案例Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Service
public class FlCaseServiceImpl implements IFlCaseService 
{
    @Autowired
    private FlCaseMapper flCaseMapper;

    /**
     * 查询案例
     * 
     * @param id 案例ID
     * @return 案例
     */
    @Override
    public FlCase selectFlCaseById(Long id)
    {
        return flCaseMapper.selectFlCaseById(id);
    }

    /**
     * 查询案例列表
     * 
     * @param flCase 案例
     * @return 案例
     */
    @Override
    public List<FlCase> selectFlCaseList(FlCase flCase)
    {
        return flCaseMapper.selectFlCaseList(flCase);
    }

    /**
     * 新增案例
     * 
     * @param flCase 案例
     * @return 结果
     */
    @Override
    public int insertFlCase(FlCase flCase)
    {
        flCase.setCreateTime(DateUtils.getNowDate());
        return flCaseMapper.insertFlCase(flCase);
    }

    /**
     * 修改案例
     * 
     * @param flCase 案例
     * @return 结果
     */
    @Override
    public int updateFlCase(FlCase flCase)
    {
        flCase.setUpdateTime(DateUtils.getNowDate());
        return flCaseMapper.updateFlCase(flCase);
    }

    /**
     * 删除案例对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFlCaseByIds(String ids)
    {
        return flCaseMapper.deleteFlCaseByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除案例信息
     * 
     * @param id 案例ID
     * @return 结果
     */
    @Override
    public int deleteFlCaseById(Long id)
    {
        return flCaseMapper.deleteFlCaseById(id);
    }
}
