package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.domain.flz.JsResume;
import com.ruoyi.system.domain.flz.TerritoryResumeDO;
import com.ruoyi.system.mapper.flz.JsResumeMapper;
import com.ruoyi.system.service.IJsResumeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 个人简介Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Service
public class JsResumeServiceImpl implements IJsResumeService 
{
    @Autowired
    private JsResumeMapper jsResumeMapper;

    /**
     * 查询个人简介
     * 
     * @param id 个人简介ID
     * @return 个人简介
     */
    @Override
    public JsResume selectJsResumeById(Long id)
    {
        return jsResumeMapper.selectJsResumeById(id);
    }

    /**
     * 查询个人简介列表
     * 
     * @param jsResume 个人简介
     * @return 个人简介
     */
    @Override
    public List<JsResume> selectJsResumeList(JsResume jsResume)
    {
        return jsResumeMapper.selectJsResumeList(jsResume);
    }

    /**
     * 新增个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    @Override
    public int insertJsResume(JsResume jsResume)
    {
        jsResume.setCreateTime(DateUtils.getNowDate());
        jsResume.setIsDelete(0);
        int in = jsResumeMapper.insertJsResume(jsResume);
        if(in>0) {
			Long[] territoryIds = jsResume.getTerritoryIds();
			for (Long long1 : territoryIds) {
				
				TerritoryResumeDO territoryResume = new TerritoryResumeDO();
				territoryResume.setResumeId(jsResume.getId());
				territoryResume.setTerritoryId(long1);
				
				jsResumeMapper.insertTerritoryResume(territoryResume);
			}
		}
        return in;
    }

    /**
     * 修改个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    @Override
    public int updateJsResume(JsResume jsResume)
    {
        jsResume.setUpdateTime(DateUtils.getNowDate());
        int i = jsResumeMapper.updateJsResume(jsResume);
        //删除关联
        jsResumeMapper.deleteTerritoryResume(jsResume.getId());
        Long[] territoryIds = jsResume.getTerritoryIds();
        for (Long long1 : territoryIds) {
			
			TerritoryResumeDO territoryResume = new TerritoryResumeDO();
			territoryResume.setResumeId(jsResume.getId());
			territoryResume.setTerritoryId(long1);
			
			jsResumeMapper.insertTerritoryResume(territoryResume);
		}
        return i;
    }

    /**
     * 删除个人简介对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJsResumeByIds(String ids)
    {
        return jsResumeMapper.deleteJsResumeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除个人简介信息
     * 
     * @param id 个人简介ID
     * @return 结果
     */
    @Override
    public int deleteJsResumeById(Long id)
    {
        return jsResumeMapper.deleteJsResumeById(id);
    }

	@Override
	public List<FlTerritory> selectTerritoryAll() {
		
		return jsResumeMapper.selectTerritoryAll();
	}

	@Override
	public List<FlTerritory> queryTerritoryResume(Long id) {
		//查询当前律师的领域
		List<TerritoryResumeDO> queryTerritoryResume = jsResumeMapper.queryTerritoryResume(id);
		
		List<FlTerritory> queryTerritoryList = jsResumeMapper.selectTerritoryAll();

		for (FlTerritory territoryDO : queryTerritoryList) {

			for (TerritoryResumeDO territoryResume : queryTerritoryResume) {

				if (territoryResume.getTerritoryId().longValue() == territoryDO.getId().longValue()) {
					territoryDO.setFlag(true);
					break;
				}
			}
		}
		return queryTerritoryList;
	}
}
