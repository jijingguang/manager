package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.domain.flz.JsResume;

/**
 * 个人简介Service接口
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public interface IJsResumeService 
{
    /**
     * 查询个人简介
     * 
     * @param id 个人简介ID
     * @return 个人简介
     */
    public JsResume selectJsResumeById(Long id);

    /**
     * 查询个人简介列表
     * 
     * @param jsResume 个人简介
     * @return 个人简介集合
     */
    public List<JsResume> selectJsResumeList(JsResume jsResume);

    /**
     * 新增个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    public int insertJsResume(JsResume jsResume);

    /**
     * 修改个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    public int updateJsResume(JsResume jsResume);

    /**
     * 批量删除个人简介
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJsResumeByIds(String ids);

    /**
     * 删除个人简介信息
     * 
     * @param id 个人简介ID
     * @return 结果
     */
    public int deleteJsResumeById(Long id);
    
    /**
     * 查询所有的领域
     */
    public List<FlTerritory> selectTerritoryAll();
    
    public List<FlTerritory> queryTerritoryResume(Long id);
}
