package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.flz.JsConsult;

/**
 * 咨询Service接口
 * 
 * @author ruoyi
 * @date 2019-12-02
 */
public interface IJsConsultService 
{
    /**
     * 查询咨询
     * 
     * @param id 咨询ID
     * @return 咨询
     */
    public JsConsult selectJsConsultById(Long id);

    /**
     * 查询咨询列表
     * 
     * @param jsConsult 咨询
     * @return 咨询集合
     */
    public List<JsConsult> selectJsConsultList(JsConsult jsConsult);

    /**
     * 新增咨询
     * 
     * @param jsConsult 咨询
     * @return 结果
     */
    public int insertJsConsult(JsConsult jsConsult);

    /**
     * 修改咨询
     * 
     * @param jsConsult 咨询
     * @return 结果
     */
    public int updateJsConsult(JsConsult jsConsult);

    /**
     * 批量删除咨询
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJsConsultByIds(String ids);

    /**
     * 删除咨询信息
     * 
     * @param id 咨询ID
     * @return 结果
     */
    public int deleteJsConsultById(Long id);
}
