package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.flz.JsArticle;

/**
 * 文章Service接口
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public interface IJsArticleService 
{
    /**
     * 查询文章
     * 
     * @param id 文章ID
     * @return 文章
     */
    public JsArticle selectJsArticleById(Long id);

    /**
     * 查询文章列表
     * 
     * @param jsArticle 文章
     * @return 文章集合
     */
    public List<JsArticle> selectJsArticleList(JsArticle jsArticle);

    /**
     * 新增文章
     * 
     * @param jsArticle 文章
     * @return 结果
     */
    public int insertJsArticle(JsArticle jsArticle);

    /**
     * 修改文章
     * 
     * @param jsArticle 文章
     * @return 结果
     */
    public int updateJsArticle(JsArticle jsArticle);

    /**
     * 批量删除文章
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJsArticleByIds(String ids);

    /**
     * 删除文章信息
     * 
     * @param id 文章ID
     * @return 结果
     */
    public int deleteJsArticleById(Long id);
}
