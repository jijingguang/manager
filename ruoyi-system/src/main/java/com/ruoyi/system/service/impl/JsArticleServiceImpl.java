package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.flz.JsArticle;
import com.ruoyi.system.mapper.flz.JsArticleMapper;
import com.ruoyi.system.service.IJsArticleService;

/**
 * 文章Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Service
public class JsArticleServiceImpl implements IJsArticleService 
{
    @Autowired
    private JsArticleMapper jsArticleMapper;

    /**
     * 查询文章
     * 
     * @param id 文章ID
     * @return 文章
     */
    @Override
    public JsArticle selectJsArticleById(Long id)
    {
        return jsArticleMapper.selectJsArticleById(id);
    }

    /**
     * 查询文章列表
     * 
     * @param jsArticle 文章
     * @return 文章
     */
    @Override
    public List<JsArticle> selectJsArticleList(JsArticle jsArticle)
    {
        return jsArticleMapper.selectJsArticleList(jsArticle);
    }

    /**
     * 新增文章
     * 
     * @param jsArticle 文章
     * @return 结果
     */
    @Override
    public int insertJsArticle(JsArticle jsArticle)
    {
        jsArticle.setCreateTime(DateUtils.getNowDate());
        return jsArticleMapper.insertJsArticle(jsArticle);
    }

    /**
     * 修改文章
     * 
     * @param jsArticle 文章
     * @return 结果
     */
    @Override
    public int updateJsArticle(JsArticle jsArticle)
    {
        jsArticle.setUpdateTime(DateUtils.getNowDate());
        return jsArticleMapper.updateJsArticle(jsArticle);
    }

    /**
     * 删除文章对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJsArticleByIds(String ids)
    {
        return jsArticleMapper.deleteJsArticleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文章信息
     * 
     * @param id 文章ID
     * @return 结果
     */
    @Override
    public int deleteJsArticleById(Long id)
    {
        return jsArticleMapper.deleteJsArticleById(id);
    }
}
