package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.flz.FlCase;

/**
 * 案例Service接口
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public interface IFlCaseService 
{
    /**
     * 查询案例
     * 
     * @param id 案例ID
     * @return 案例
     */
    public FlCase selectFlCaseById(Long id);

    /**
     * 查询案例列表
     * 
     * @param flCase 案例
     * @return 案例集合
     */
    public List<FlCase> selectFlCaseList(FlCase flCase);

    /**
     * 新增案例
     * 
     * @param flCase 案例
     * @return 结果
     */
    public int insertFlCase(FlCase flCase);

    /**
     * 修改案例
     * 
     * @param flCase 案例
     * @return 结果
     */
    public int updateFlCase(FlCase flCase);

    /**
     * 批量删除案例
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFlCaseByIds(String ids);

    /**
     * 删除案例信息
     * 
     * @param id 案例ID
     * @return 结果
     */
    public int deleteFlCaseById(Long id);
}
