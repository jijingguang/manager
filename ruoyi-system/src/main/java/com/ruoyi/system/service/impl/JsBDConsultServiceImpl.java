package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.flz.JsConsult;
import com.ruoyi.system.mapper.flz.JsBDConsultMapper;
import com.ruoyi.system.service.IJsBDConsultService;

/**
 * 咨询Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-12-02
 */
@Service
public class JsBDConsultServiceImpl implements IJsBDConsultService 
{
    @Autowired
    private JsBDConsultMapper jsConsultMapper;

    /**
     * 查询咨询
     * 
     * @param id 咨询ID
     * @return 咨询
     */
    @Override
    public JsConsult selectJsConsultById(Long id)
    {
        return jsConsultMapper.selectJsConsultById(id);
    }

    /**
     * 查询咨询列表
     * 
     * @param jsConsult 咨询
     * @return 咨询
     */
    @Override
    public List<JsConsult> selectJsConsultList(JsConsult jsConsult)
    {
        return jsConsultMapper.selectJsConsultList(jsConsult);
    }

    /**
     * 新增咨询
     * 
     * @param jsConsult 咨询
     * @return 结果
     */
    @Override
    public int insertJsConsult(JsConsult jsConsult)
    {
        jsConsult.setCreateTime(DateUtils.getNowDate());
        return jsConsultMapper.insertJsConsult(jsConsult);
    }

    /**
     * 修改咨询
     * 
     * @param jsConsult 咨询
     * @return 结果
     */
    @Override
    public int updateJsConsult(JsConsult jsConsult)
    {
        return jsConsultMapper.updateJsConsult(jsConsult);
    }

    /**
     * 删除咨询对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJsConsultByIds(String ids)
    {
        return jsConsultMapper.deleteJsConsultByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除咨询信息
     * 
     * @param id 咨询ID
     * @return 结果
     */
    @Override
    public int deleteJsConsultById(Long id)
    {
        return jsConsultMapper.deleteJsConsultById(id);
    }
}
