package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.mapper.flz.FlTerritoryMapper;
import com.ruoyi.system.service.IFlTerritoryService;
import com.ruoyi.common.core.text.Convert;

/**
 * 领域Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Service
public class FlTerritoryServiceImpl implements IFlTerritoryService 
{
    @Autowired
    private FlTerritoryMapper flTerritoryMapper;

    /**
     * 查询领域
     * 
     * @param id 领域ID
     * @return 领域
     */
    @Override
    public FlTerritory selectFlTerritoryById(Long id)
    {
        return flTerritoryMapper.selectFlTerritoryById(id);
    }

    /**
     * 查询领域列表
     * 
     * @param flTerritory 领域
     * @return 领域
     */
    @Override
    public List<FlTerritory> selectFlTerritoryList(FlTerritory flTerritory)
    {
        return flTerritoryMapper.selectFlTerritoryList(flTerritory);
    }

    /**
     * 新增领域
     * 
     * @param flTerritory 领域
     * @return 结果
     */
    @Override
    public int insertFlTerritory(FlTerritory flTerritory)
    {
        flTerritory.setCreateTime(DateUtils.getNowDate());
        return flTerritoryMapper.insertFlTerritory(flTerritory);
    }

    /**
     * 修改领域
     * 
     * @param flTerritory 领域
     * @return 结果
     */
    @Override
    public int updateFlTerritory(FlTerritory flTerritory)
    {
        flTerritory.setUpdateTime(DateUtils.getNowDate());
        return flTerritoryMapper.updateFlTerritory(flTerritory);
    }

    /**
     * 删除领域对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFlTerritoryByIds(String ids)
    {
        return flTerritoryMapper.deleteFlTerritoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除领域信息
     * 
     * @param id 领域ID
     * @return 结果
     */
    @Override
    public int deleteFlTerritoryById(Long id)
    {
        return flTerritoryMapper.deleteFlTerritoryById(id);
    }

    /**
     * 查询领域树列表
     * 
     * @return 所有领域信息
     */
    @Override
    public List<Ztree> selectFlTerritoryTree()
    {
        List<FlTerritory> flTerritoryList = flTerritoryMapper.selectFlTerritoryList(new FlTerritory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (FlTerritory flTerritory : flTerritoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(flTerritory.getId());
            ztree.setpId(flTerritory.getParentId());
            ztree.setName(flTerritory.getTerritoryName());
            ztree.setTitle(flTerritory.getTerritoryName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
