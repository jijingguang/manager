package com.ruoyi.system.domain.flz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 案例对象 fl_case
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public class FlCase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String caseTitle;

    /** 类型id */
    @Excel(name = "类型id")
    private Long categoryId;

    /** 法院 */
    @Excel(name = "法院")
    private String courtName;

    /** 判决形式 */
    @Excel(name = "判决形式")
    private String sentenceType;

    /** 标的额 */
    @Excel(name = "标的额")
    private String targetAmount;
    
    /** 描述 */
    @Excel(name = "描述")
    private String caseDepict;

    /** 内容 */
    @Excel(name = "内容")
    private String catDesc;

    /** 展示 0:否 1:是 */
    @Excel(name = "展示 0:否 1:是")
    private Integer isShow;

    /** 是否删除 0:正常 1:删除 */
    @Excel(name = "是否删除 0:正常 1:删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCaseTitle(String caseTitle) 
    {
        this.caseTitle = caseTitle;
    }

    public String getCaseTitle() 
    {
        return caseTitle;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setCourtName(String courtName) 
    {
        this.courtName = courtName;
    }

    public String getCourtName() 
    {
        return courtName;
    }
    public void setSentenceType(String sentenceType) 
    {
        this.sentenceType = sentenceType;
    }

    public String getSentenceType() 
    {
        return sentenceType;
    }
    public void setTargetAmount(String targetAmount) 
    {
        this.targetAmount = targetAmount;
    }

    public String getTargetAmount() 
    {
        return targetAmount;
    }
    
    public String getCaseDepict() {
		return caseDepict;
	}

	public void setCaseDepict(String caseDepict) {
		this.caseDepict = caseDepict;
	}

	public void setCatDesc(String catDesc) 
    {
        this.catDesc = catDesc;
    }

    public String getCatDesc() 
    {
        return catDesc;
    }
    public void setIsShow(Integer isShow) 
    {
        this.isShow = isShow;
    }

    public Integer getIsShow() 
    {
        return isShow;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("caseTitle", getCaseTitle())
            .append("categoryId", getCategoryId())
            .append("courtName", getCourtName())
            .append("sentenceType", getSentenceType())
            .append("targetAmount", getTargetAmount())
            .append("catDesc", getCatDesc())
            .append("isShow", getIsShow())
            .append("isDelete", getIsDelete())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
