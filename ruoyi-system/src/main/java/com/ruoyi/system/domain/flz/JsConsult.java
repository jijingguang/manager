package com.ruoyi.system.domain.flz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 咨询对象 js_consult
 * 
 * @author ruoyi
 * @date 2019-12-02
 */
public class JsConsult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 住址 */
    @Excel(name = "住址")
    private String address;

    /** 案件类型 */
    @Excel(name = "案件类型")
    private String category;

    /** 案情描述 */
    @Excel(name = "案情描述")
    private String consultDescribe;

    /** 状态 0:待联系 1:已联系 */
    @Excel(name = "状态 0:待联系 1:已联系")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setConsultDescribe(String consultDescribe) 
    {
        this.consultDescribe = consultDescribe;
    }

    public String getConsultDescribe() 
    {
        return consultDescribe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("category", getCategory())
            .append("consultDescribe", getConsultDescribe())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .toString();
    }
}
