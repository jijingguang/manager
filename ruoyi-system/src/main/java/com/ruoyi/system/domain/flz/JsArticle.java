package com.ruoyi.system.domain.flz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章对象 js_article
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public class JsArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String articleTitle;

    /** 浏览器标签页标题 */
    @Excel(name = "浏览器标签页标题")
    private String labTitle;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 文章描述 */
    @Excel(name = "文章描述")
    private String articleDescribe;

    /** 类型id */
    @Excel(name = "类型id")
    private Integer categoryId;

    /** 标签id */
    @Excel(name = "标签id")
    private Integer lableId;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keywords;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imageUrl;

    /** 浏览数量 */
    @Excel(name = "浏览数量")
    private Long clickNum;

    /** 作者 */
    @Excel(name = "作者")
    private String author;

    /** 是否开启 0:开启 1:关闭 */
    @Excel(name = "是否开启 0:开启 1:关闭")
    private Integer isOpen;

    /** 是否删除 0:正常 1:删除 */
    @Excel(name = "是否删除 0:正常 1:删除")
    private Integer isDelete;

    /** 创建人id */
    @Excel(name = "创建人id")
    private Integer created;

    /** 最近修改人id */
    @Excel(name = "最近修改人id")
    private Integer modified;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setArticleTitle(String articleTitle) 
    {
        this.articleTitle = articleTitle;
    }

    public String getArticleTitle() 
    {
        return articleTitle;
    }
    public void setLabTitle(String labTitle) 
    {
        this.labTitle = labTitle;
    }

    public String getLabTitle() 
    {
        return labTitle;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setArticleDescribe(String articleDescribe) 
    {
        this.articleDescribe = articleDescribe;
    }

    public String getArticleDescribe() 
    {
        return articleDescribe;
    }
    public void setCategoryId(Integer categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Integer getCategoryId() 
    {
        return categoryId;
    }
    public void setLableId(Integer lableId) 
    {
        this.lableId = lableId;
    }

    public Integer getLableId() 
    {
        return lableId;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setImageUrl(String imageUrl) 
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() 
    {
        return imageUrl;
    }
    public void setClickNum(Long clickNum) 
    {
        this.clickNum = clickNum;
    }

    public Long getClickNum() 
    {
        return clickNum;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setIsOpen(Integer isOpen) 
    {
        this.isOpen = isOpen;
    }

    public Integer getIsOpen() 
    {
        return isOpen;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }
    public void setCreated(Integer created) 
    {
        this.created = created;
    }

    public Integer getCreated() 
    {
        return created;
    }
    public void setModified(Integer modified) 
    {
        this.modified = modified;
    }

    public Integer getModified() 
    {
        return modified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("articleTitle", getArticleTitle())
            .append("labTitle", getLabTitle())
            .append("content", getContent())
            .append("articleDescribe", getArticleDescribe())
            .append("categoryId", getCategoryId())
            .append("lableId", getLableId())
            .append("keywords", getKeywords())
            .append("imageUrl", getImageUrl())
            .append("clickNum", getClickNum())
            .append("author", getAuthor())
            .append("isOpen", getIsOpen())
            .append("isDelete", getIsDelete())
            .append("created", getCreated())
            .append("modified", getModified())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
