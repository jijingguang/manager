package com.ruoyi.system.domain.flz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 个人简介对象 js_resume
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public class JsResume extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String englishName;

    /** 职位 */
    @Excel(name = "职位")
    private Integer position;
    
    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 介绍 */
    @Excel(name = "介绍")
    private String recommend;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imageUrl;

    /** 是否删除 0:正常 1:删除 */
    @Excel(name = "是否删除 0:正常 1:删除")
    private Integer isDelete;
    
    private Long[] territoryIds;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setEnglishName(String englishName) 
    {
        this.englishName = englishName;
    }

    public String getEnglishName() 
    {
        return englishName;
    }
    public void setPosition(Integer position) 
    {
        this.position = position;
    }

    public Integer getPosition() 
    {
        return position;
    }
    
    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRecommend(String recommend) 
    {
        this.recommend = recommend;
    }

    public String getRecommend() 
    {
        return recommend;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setImageUrl(String imageUrl) 
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() 
    {
        return imageUrl;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    public Long[] getTerritoryIds() {
		return territoryIds;
	}

	public void setTerritoryIds(Long[] territoryIds) {
		this.territoryIds = territoryIds;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("englishName", getEnglishName())
            .append("position", getPosition())
            .append("recommend", getRecommend())
            .append("address", getAddress())
            .append("imageUrl", getImageUrl())
            .append("isDelete", getIsDelete())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
