package com.ruoyi.system.domain.flz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 领域对象 fl_territory
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public class FlTerritory extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** 父ID */
    @Excel(name = "父ID")
    private Long parentId;

    /** 领域名称 */
    @Excel(name = "领域名称")
    private String territoryName;

    /** 描述 */
    @Excel(name = "描述")
    private String catDesc;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imageUrl;

    /** 是否删除 0:正常 1:删除 */
    @Excel(name = "是否删除 0:正常 1:删除")
    private Integer isDelete;
    
    /** 用户是否存在此岗位标识 默认不存在 */
    private boolean flag = false;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setTerritoryName(String territoryName) 
    {
        this.territoryName = territoryName;
    }

    public String getTerritoryName() 
    {
        return territoryName;
    }
    public void setCatDesc(String catDesc) 
    {
        this.catDesc = catDesc;
    }

    public String getCatDesc() 
    {
        return catDesc;
    }
    public void setImageUrl(String imageUrl) 
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() 
    {
        return imageUrl;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("territoryName", getTerritoryName())
            .append("catDesc", getCatDesc())
            .append("imageUrl", getImageUrl())
            .append("isDelete", getIsDelete())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
