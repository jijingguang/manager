package com.ruoyi.system.mapper.flz;

import java.util.List;

import com.ruoyi.system.domain.flz.FlTerritory;

/**
 * 领域Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public interface FlTerritoryMapper 
{
    /**
     * 查询领域
     * 
     * @param id 领域ID
     * @return 领域
     */
    public FlTerritory selectFlTerritoryById(Long id);

    /**
     * 查询领域列表
     * 
     * @param flTerritory 领域
     * @return 领域集合
     */
    public List<FlTerritory> selectFlTerritoryList(FlTerritory flTerritory);

    /**
     * 新增领域
     * 
     * @param flTerritory 领域
     * @return 结果
     */
    public int insertFlTerritory(FlTerritory flTerritory);

    /**
     * 修改领域
     * 
     * @param flTerritory 领域
     * @return 结果
     */
    public int updateFlTerritory(FlTerritory flTerritory);

    /**
     * 删除领域
     * 
     * @param id 领域ID
     * @return 结果
     */
    public int deleteFlTerritoryById(Long id);

    /**
     * 批量删除领域
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFlTerritoryByIds(String[] ids);
}
