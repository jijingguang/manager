package com.ruoyi.system.mapper.flz;

import java.util.List;

import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.domain.flz.JsResume;
import com.ruoyi.system.domain.flz.TerritoryResumeDO;

/**
 * 个人简介Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
public interface JsResumeMapper 
{
    /**
     * 查询个人简介
     * 
     * @param id 个人简介ID
     * @return 个人简介
     */
    public JsResume selectJsResumeById(Long id);

    /**
     * 查询个人简介列表
     * 
     * @param jsResume 个人简介
     * @return 个人简介集合
     */
    public List<JsResume> selectJsResumeList(JsResume jsResume);

    /**
     * 新增个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    public int insertJsResume(JsResume jsResume);
    
    /**
     * 律师与领域关联
     */
    public int insertTerritoryResume(TerritoryResumeDO territoryResume);

    /**
     * 修改个人简介
     * 
     * @param jsResume 个人简介
     * @return 结果
     */
    public int updateJsResume(JsResume jsResume);
    
    /**
     * 删除某个律师的关联领域
     */
    public int deleteTerritoryResume(Long id);

    /**
     * 删除个人简介
     * 
     * @param id 个人简介ID
     * @return 结果
     */
    public int deleteJsResumeById(Long id);

    /**
     * 批量删除个人简介
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJsResumeByIds(String[] ids);
    
    /**
     * 查询所有的领域
     */
    public List<FlTerritory> selectTerritoryAll();
    
    public List<TerritoryResumeDO> queryTerritoryResume(Long id);
}

