package com.ruoyi.web.controller.flz;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.flz.FlCase;
import com.ruoyi.system.service.IFlCaseService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 案例Controller
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Controller
@RequestMapping("/system/case")
public class FlCaseController extends BaseController
{
    private String prefix = "flz/case";

    @Autowired
    private IFlCaseService flCaseService;

    @RequiresPermissions("system:case:view")
    @GetMapping()
    public String caseList()
    {
        return prefix + "/case";
    }

    /**
     * 查询案例列表
     */
    @RequiresPermissions("system:case:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FlCase flCase)
    {
        startPage();
        List<FlCase> list = flCaseService.selectFlCaseList(flCase);
        return getDataTable(list);
    }

    /**
     * 导出案例列表
     */
    @RequiresPermissions("system:case:export")
    @Log(title = "案例", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FlCase flCase)
    {
        List<FlCase> list = flCaseService.selectFlCaseList(flCase);
        ExcelUtil<FlCase> util = new ExcelUtil<FlCase>(FlCase.class);
        return util.exportExcel(list, "case");
    }

    /**
     * 新增案例
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存案例
     */
    @RequiresPermissions("system:case:add")
    @Log(title = "案例", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FlCase flCase)
    {
        return toAjax(flCaseService.insertFlCase(flCase));
    }

    /**
     * 修改案例
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        FlCase flCase = flCaseService.selectFlCaseById(id);
        mmap.put("flCase", flCase);
        return prefix + "/edit";
    }

    /**
     * 修改保存案例
     */
    @RequiresPermissions("system:case:edit")
    @Log(title = "案例", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FlCase flCase)
    {
        return toAjax(flCaseService.updateFlCase(flCase));
    }

    /**
     * 删除案例
     */
    @RequiresPermissions("system:case:remove")
    @Log(title = "案例", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(flCaseService.deleteFlCaseByIds(ids));
    }
}
