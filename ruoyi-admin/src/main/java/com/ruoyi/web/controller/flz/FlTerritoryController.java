package com.ruoyi.web.controller.flz;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.service.IFlTerritoryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 领域Controller
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Controller
@RequestMapping("/system/territory")
public class FlTerritoryController extends BaseController
{
    private String prefix = "flz/territory";

    @Autowired
    private IFlTerritoryService flTerritoryService;

    @RequiresPermissions("system:territory:view")
    @GetMapping()
    public String territory()
    {
        return prefix + "/territory";
    }

    /**
     * 查询领域树列表
     */
    @RequiresPermissions("system:territory:list")
    @PostMapping("/list")
    @ResponseBody
    public List<FlTerritory> list(FlTerritory flTerritory)
    {
        List<FlTerritory> list = flTerritoryService.selectFlTerritoryList(flTerritory);
        return list;
    }

    /**
     * 导出领域列表
     */
    @RequiresPermissions("system:territory:export")
    @Log(title = "领域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FlTerritory flTerritory)
    {
        List<FlTerritory> list = flTerritoryService.selectFlTerritoryList(flTerritory);
        ExcelUtil<FlTerritory> util = new ExcelUtil<FlTerritory>(FlTerritory.class);
        return util.exportExcel(list, "territory");
    }

    /**
     * 新增领域
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("flTerritory", flTerritoryService.selectFlTerritoryById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存领域
     */
    @RequiresPermissions("system:territory:add")
    @Log(title = "领域", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FlTerritory flTerritory)
    {
        return toAjax(flTerritoryService.insertFlTerritory(flTerritory));
    }

    /**
     * 修改领域
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        FlTerritory flTerritory = flTerritoryService.selectFlTerritoryById(id);
        mmap.put("flTerritory", flTerritory);
        return prefix + "/edit";
    }

    /**
     * 修改保存领域
     */
    @RequiresPermissions("system:territory:edit")
    @Log(title = "领域", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FlTerritory flTerritory)
    {
        return toAjax(flTerritoryService.updateFlTerritory(flTerritory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:territory:remove")
    @Log(title = "领域", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(flTerritoryService.deleteFlTerritoryById(id));
    }

    /**
     * 选择领域树
     */
    @GetMapping(value = { "/selectTerritoryTree/{id}", "/selectTerritoryTree/" })
    public String selectTerritoryTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("flTerritory", flTerritoryService.selectFlTerritoryById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载领域树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = flTerritoryService.selectFlTerritoryTree();
        return ztrees;
    }
}
