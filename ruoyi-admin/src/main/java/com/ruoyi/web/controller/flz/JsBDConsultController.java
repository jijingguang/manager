package com.ruoyi.web.controller.flz;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.flz.JsConsult;
import com.ruoyi.system.service.IJsBDConsultService;

/**
 * 刑事咨询Controller
 * 
 */
@Controller
@RequestMapping("/system/baiduConsult")
public class JsBDConsultController extends BaseController
{
    private String prefix = "flz/consult";

    @Autowired
    private IJsBDConsultService jsConsultService;

    @RequiresPermissions("system:baiduConsult:view")
    @GetMapping()
    public String consult()
    {
        return prefix + "/baiduconsult";
    }

    /**
     * 查询咨询列表
     */
    @RequiresPermissions("system:baiduConsult:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(JsConsult jsConsult)
    {
        startPage();
        List<JsConsult> list = jsConsultService.selectJsConsultList(jsConsult);
        return getDataTable(list);
    }

    /**
     * 导出咨询列表
     */
    @RequiresPermissions("system:baiduConsult:export")
    @Log(title = "咨询", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(JsConsult jsConsult)
    {
        List<JsConsult> list = jsConsultService.selectJsConsultList(jsConsult);
        ExcelUtil<JsConsult> util = new ExcelUtil<JsConsult>(JsConsult.class);
        return util.exportExcel(list, "consult");
    }

    /**
     * 新增咨询
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存咨询
     */
    @RequiresPermissions("system:baiduConsult:add")
    @Log(title = "咨询", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(JsConsult jsConsult)
    {
        return toAjax(jsConsultService.insertJsConsult(jsConsult));
    }

    /**
     * 修改咨询
     */
    @ResponseBody
    @RequestMapping("/updateStatus")
    public AjaxResult edit( Long id)
    {
    	JsConsult jsConsult = new JsConsult();
    	jsConsult.setId(id);
    	jsConsult.setStatus(1);
    	jsConsultService.updateJsConsult(jsConsult);
        return AjaxResult.success();
    }
    
    /**
     * 修改咨询
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        JsConsult jsConsult = jsConsultService.selectJsConsultById(id);
        mmap.put("jsConsult", jsConsult);
        return prefix + "/edit";
    }


    /**
     * 删除咨询
     */
    @RequiresPermissions("system:baiduConsult:remove")
    @Log(title = "咨询", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(jsConsultService.deleteJsConsultByIds(ids));
    }
}
