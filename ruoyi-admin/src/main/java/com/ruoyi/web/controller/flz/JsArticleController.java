package com.ruoyi.web.controller.flz;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.BootdoConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.FileType;
import com.ruoyi.common.utils.FileUtil;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.FileDO;
import com.ruoyi.system.domain.flz.JsArticle;
import com.ruoyi.system.service.IJsArticleService;

/**
 * 文章Controller
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Controller
@RequestMapping("/system/article")
public class JsArticleController extends BaseController
{
    private String prefix = "flz/article";

    @Autowired
    private IJsArticleService jsArticleService;
    
    @Autowired
	private BootdoConfig bootdoConfig;

    @RequiresPermissions("system:article:view")
    @GetMapping()
    public String article()
    {
        return prefix + "/article";
    }

    /**
     * 查询文章列表
     */
    @RequiresPermissions("system:article:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(JsArticle jsArticle)
    {
        startPage();
        List<JsArticle> list = jsArticleService.selectJsArticleList(jsArticle);
        return getDataTable(list);
    }

    /**
     * 导出文章列表
     */
    @RequiresPermissions("system:article:export")
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(JsArticle jsArticle)
    {
        List<JsArticle> list = jsArticleService.selectJsArticleList(jsArticle);
        ExcelUtil<JsArticle> util = new ExcelUtil<JsArticle>(JsArticle.class);
        return util.exportExcel(list, "article");
    }

    /**
     * 新增文章
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存文章
     */
    @RequiresPermissions("system:article:add")
    @Log(title = "文章", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(JsArticle jsArticle)
    {
        return toAjax(jsArticleService.insertJsArticle(jsArticle));
    }

    /**
     * 修改文章
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        JsArticle jsArticle = jsArticleService.selectJsArticleById(id);
        mmap.put("jsArticle", jsArticle);
        return prefix + "/edit";
    }

    /**
     * 修改保存文章
     */
    @RequiresPermissions("system:article:edit")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JsArticle jsArticle)
    {
        return toAjax(jsArticleService.updateJsArticle(jsArticle));
    }

    /**
     * 删除文章
     */
    @RequiresPermissions("system:article:remove")
    @Log(title = "文章", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(jsArticleService.deleteJsArticleByIds(ids));
    }
    
    @ResponseBody
	@PostMapping("/upload")
	public AjaxResult upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();
		fileName = FileUtil.renameToUUID(fileName);
		FileDO sysFile = new FileDO(FileType.fileType(fileName), "/files/" + fileName, new Date());
		try {
			FileUtil.uploadFile(file.getBytes(), bootdoConfig.getUploadPath(), fileName);
		} catch (Exception e) {
			return AjaxResult.warn("上传失败！");
		}
		return AjaxResult.success(sysFile.getUrl());
	}
}
