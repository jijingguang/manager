package com.ruoyi.web.controller.flz;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.flz.FlTerritory;
import com.ruoyi.system.domain.flz.JsResume;
import com.ruoyi.system.service.IJsResumeService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 个人简介Controller
 * 
 * @author ruoyi
 * @date 2019-11-28
 */
@Controller
@RequestMapping("/system/resume")
public class JsResumeController extends BaseController
{
    private String prefix = "flz/resume";

    @Autowired
    private IJsResumeService jsResumeService;

    @RequiresPermissions("system:resume:view")
    @GetMapping()
    public String resume()
    {
        return prefix + "/resume";
    }

    /**
     * 查询个人简介列表
     */
    @RequiresPermissions("system:resume:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(JsResume jsResume)
    {
        startPage();
        List<JsResume> list = jsResumeService.selectJsResumeList(jsResume);
        return getDataTable(list);
    }

    /**
     * 导出个人简介列表
     */
    @RequiresPermissions("system:resume:export")
    @Log(title = "个人简介", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(JsResume jsResume)
    {
        List<JsResume> list = jsResumeService.selectJsResumeList(jsResume);
        ExcelUtil<JsResume> util = new ExcelUtil<JsResume>(JsResume.class);
        return util.exportExcel(list, "resume");
    }

    /**
     * 新增个人简介
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
    	mmap.put("territoryList", jsResumeService.selectTerritoryAll());
        return prefix + "/add";
    }

    /**
     * 新增保存个人简介
     */
    @RequiresPermissions("system:resume:add")
    @Log(title = "个人简介", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(JsResume jsResume)
    {
        return toAjax(jsResumeService.insertJsResume(jsResume));
    }

    /**
     * 修改个人简介
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        JsResume jsResume = jsResumeService.selectJsResumeById(id);
        List<FlTerritory> list = jsResumeService.queryTerritoryResume(id);
        mmap.put("jsResume", jsResume);
        mmap.put("list", list);
        return prefix + "/edit";
    }

    /**
     * 修改保存个人简介
     */
    @RequiresPermissions("system:resume:edit")
    @Log(title = "个人简介", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(JsResume jsResume)
    {
        return toAjax(jsResumeService.updateJsResume(jsResume));
    }

    /**
     * 删除个人简介
     */
    @RequiresPermissions("system:resume:remove")
    @Log(title = "个人简介", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(jsResumeService.deleteJsResumeByIds(ids));
    }
}
