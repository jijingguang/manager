﻿/*
Navicat MySQL Data Transfer

Source Server         : 北京
Source Server Version : 50615
Source Host           : 39.106.213.243:3306
Source Database       : ryfl

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2019-12-05 15:42:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fl_case
-- ----------------------------
DROP TABLE IF EXISTS `fl_case`;
CREATE TABLE `fl_case` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `case_title` varchar(255) DEFAULT NULL COMMENT '标题',
  `category_id` bigint(20) DEFAULT '0' COMMENT '类型id',
  `court_name` varchar(20) DEFAULT NULL COMMENT '法院',
  `sentence_type` varchar(20) DEFAULT NULL COMMENT '判决形式',
  `target_amount` varchar(20) DEFAULT NULL COMMENT '标的额',
  `case_depict` varchar(255) DEFAULT NULL COMMENT '描述',
  `cat_desc` longtext COMMENT '内容',
  `is_show` tinyint(1) DEFAULT '0' COMMENT '展示 0:否 1:是',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除 0:正常 1:删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='案例表';

-- ----------------------------
-- Records of fl_case
-- ----------------------------

-- ----------------------------
-- Table structure for fl_territory
-- ----------------------------
DROP TABLE IF EXISTS `fl_territory`;
CREATE TABLE `fl_territory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父ID',
  `territory_name` varchar(20) DEFAULT NULL COMMENT '领域名称',
  `cat_desc` longtext DEFAULT NULL COMMENT '描述',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除 0:正常 1:删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='领域表';

-- ----------------------------
-- Records of fl_territory
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('1', '2', 'id', null, 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('2', '2', 'name', '姓名', 'varchar(6)', 'String', 'name', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('3', '2', 'english_name', '英文名称', 'varchar(25)', 'String', 'englishName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '3', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('4', '2', 'position', '职位', 'tinyint(1)', 'Integer', 'position', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('5', '2', 'recommend', '介绍', 'longtext', 'String', 'recommend', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('6', '2', 'address', '地址', 'varchar(100)', 'String', 'address', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('7', '2', 'image_url', '图片路径', 'varchar(255)', 'String', 'imageUrl', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('8', '2', 'is_delete', '是否删除 0:正常 1:删除', 'tinyint(1)', 'Integer', 'isDelete', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('9', '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '9', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('10', '2', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '10', 'admin', '2019-11-28 09:58:17', '', null);
INSERT INTO `gen_table_column` VALUES ('11', '3', 'id', null, 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('12', '3', 'case_title', '标题', 'varchar(255)', 'String', 'caseTitle', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('13', '3', 'category_id', '类型id', 'bigint(20)', 'Long', 'categoryId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('14', '3', 'court_name', '法院', 'varchar(20)', 'String', 'courtName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '4', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('15', '3', 'sentence_type', '判决形式', 'varchar(20)', 'String', 'sentenceType', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', '', '5', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('16', '3', 'target_amount', '标的额', 'varchar(20)', 'String', 'targetAmount', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('17', '3', 'cat_desc', '描述', 'longtext', 'String', 'catDesc', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('18', '3', 'is_show', '展示 0:否 1:是', 'tinyint(1)', 'Integer', 'isShow', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('19', '3', 'is_delete', '是否删除 0:正常 1:删除', 'tinyint(1)', 'Integer', 'isDelete', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('20', '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '10', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('21', '3', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '11', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('22', '4', 'id', 'null', 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('23', '4', 'parent_id', '父ID', 'bigint(20)', 'Long', 'parentId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('24', '4', 'territory_name', '领域名称', 'varchar(20)', 'String', 'territoryName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '3', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('25', '4', 'cat_desc', '描述', 'varchar(255)', 'String', 'catDesc', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('26', '4', 'image_url', '图片路径', 'varchar(255)', 'String', 'imageUrl', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('27', '4', 'is_delete', '是否删除 0:正常 1:删除', 'tinyint(1)', 'Integer', 'isDelete', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('28', '4', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '7', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('29', '4', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '8', 'admin', '2019-11-28 10:30:23', null, '2019-11-28 10:31:22');
INSERT INTO `gen_table_column` VALUES ('30', '5', 'id', null, 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('31', '5', 'article_title', '文章标题', 'varchar(255)', 'String', 'articleTitle', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('32', '5', 'lab_title', '浏览器标签页标题', 'varchar(255)', 'String', 'labTitle', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('33', '5', 'content', '内容', 'longtext', 'String', 'content', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('34', '5', 'article_describe', '文章描述', 'varchar(255)', 'String', 'articleDescribe', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('35', '5', 'category_id', '类型id', 'int(10)', 'Integer', 'categoryId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('36', '5', 'lable_id', '标签id', 'int(10)', 'Integer', 'lableId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('37', '5', 'keywords', '关键字', 'varchar(255)', 'String', 'keywords', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('38', '5', 'image_url', '图片路径', 'varchar(255)', 'String', 'imageUrl', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('39', '5', 'click_num', '浏览数量', 'bigint(20)', 'Long', 'clickNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '10', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('40', '5', 'author', '作者', 'varchar(10)', 'String', 'author', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '11', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('41', '5', 'is_open', '是否开启 0:开启 1:关闭', 'tinyint(1)', 'Integer', 'isOpen', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '12', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('42', '5', 'is_delete', '是否删除 0:正常 1:删除', 'tinyint(1)', 'Integer', 'isDelete', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '13', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('43', '5', 'created', '创建人id', 'int(10)', 'Integer', 'created', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '14', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('44', '5', 'modified', '最近修改人id', 'int(10)', 'Integer', 'modified', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '15', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('45', '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '16', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('46', '5', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '17', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('47', '6', 'id', null, 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('48', '6', 'name', '姓名', 'varchar(6)', 'String', 'name', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('49', '6', 'mobile', '手机号', 'varchar(11)', 'String', 'mobile', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('50', '6', 'address', '住址', 'varchar(255)', 'String', 'address', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('51', '6', 'category', '案件类型', 'varchar(255)', 'String', 'category', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('52', '6', 'consult_describe', '案情描述', 'varchar(255)', 'String', 'consultDescribe', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('53', '6', 'status', '状态 0:待联系 1:已联系', 'int(1)', 'Integer', 'status', '0', '0', null, '1', '1', '1', '1', 'EQ', 'radio', '', '7', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('54', '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '8', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('55', '7', 'id', null, 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('56', '7', 'type', '图片类型 1:轮播图 2:其他', 'int(11)', 'Long', 'type', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', '', '2', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('57', '7', 'url', 'URL地址', 'varchar(200)', 'String', 'url', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2019-11-28 10:30:23', '', null);
INSERT INTO `gen_table_column` VALUES ('58', '7', 'create_date', '创建时间', 'datetime', 'Date', 'createDate', '0', '0', null, '1', '1', '1', '1', 'EQ', 'datetime', '', '4', 'admin', '2019-11-28 10:30:23', '', null);

-- ----------------------------
-- Table structure for js_article
-- ----------------------------
DROP TABLE IF EXISTS `js_article`;
CREATE TABLE `js_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `lab_title` varchar(255) DEFAULT NULL COMMENT '浏览器标签页标题',
  `content` longtext DEFAULT NULL COMMENT '内容',
  `article_describe` varchar(255) DEFAULT NULL COMMENT '文章描述',
  `category_id` int(10) NOT NULL COMMENT '类型id',
  `lable_id` int(10) DEFAULT NULL COMMENT '标签id',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `click_num` bigint(20) DEFAULT '0' COMMENT '浏览数量',
  `author` varchar(10) DEFAULT NULL COMMENT '作者',
  `is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启 0:开启 1:关闭',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除 0:正常 1:删除',
  `created` int(10) DEFAULT NULL COMMENT '创建人id',
  `modified` int(10) DEFAULT NULL COMMENT '最近修改人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of js_article
-- ----------------------------

-- ----------------------------
-- Table structure for js_article_category
-- ----------------------------
DROP TABLE IF EXISTS `js_article_category`;
CREATE TABLE `js_article_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父ID',
  `category_name` varchar(20) DEFAULT NULL COMMENT '类别名称',
  `cat_desc` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `lab_title` varchar(255) DEFAULT NULL COMMENT '标题',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `show_in_nav` tinyint(1) DEFAULT '0' COMMENT '是否导航显示 0:不显示 1:显示',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除 0:正常 1:删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of js_article_category
-- ----------------------------

-- ----------------------------
-- Table structure for js_consult
-- ----------------------------
DROP TABLE IF EXISTS `js_consult`;
CREATE TABLE `js_consult` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(6) DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `address` varchar(255) DEFAULT NULL COMMENT '住址',
  `category` varchar(255) DEFAULT NULL COMMENT '案件类型',
  `consult_describe` varchar(255) DEFAULT NULL COMMENT '案情描述',
  `status` int(1) DEFAULT '0' COMMENT '状态 0:待联系 1:已联系',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='咨询表';

-- ----------------------------
-- Records of js_consult
-- ----------------------------

-- ----------------------------
-- Table structure for js_image
-- ----------------------------
DROP TABLE IF EXISTS `js_image`;
CREATE TABLE `js_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '图片类型 1:轮播图 2:其他',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of js_image
-- ----------------------------

-- ----------------------------
-- Table structure for js_resume
-- ----------------------------
DROP TABLE IF EXISTS `js_resume`;
CREATE TABLE `js_resume` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(6) DEFAULT NULL COMMENT '姓名',
  `english_name` varchar(25) DEFAULT NULL COMMENT '英文名称',
  `position` tinyint(1) DEFAULT '0' COMMENT '职位',
  `email` varchar(25) DEFAULT NULL COMMENT '邮箱',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `recommend` longtext COMMENT '介绍',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除 0:正常 1:删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='个人简介表';

-- ----------------------------
-- Records of js_resume
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'localhost1575516270491', '1575531698417', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', null, '1575516270000', '-1', '5', 'PAUSED', 'CRON', '1575516270000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', null, '1575516285000', '-1', '5', 'PAUSED', 'CRON', '1575516271000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', null, '1575516280000', '-1', '5', 'PAUSED', 'CRON', '1575516271000', '0', null, '2', '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '若依科技', '0', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '深圳总公司', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '长沙分公司', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '研发部门', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '市场部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '测试部门', '3', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('106', '101', '0,100,101', '财务部门', '4', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('107', '101', '0,100,101', '运维部门', '5', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('108', '102', '0,100,102', '市场部门', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('109', '102', '0,100,102', '财务部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES ('19', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES ('20', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES ('21', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES ('22', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES ('23', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES ('24', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES ('25', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES ('26', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES ('27', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('28', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('100', '1', '部门主任', '1', 'flz_lawyer_ position', null, 'default', 'Y', '0', 'admin', '2019-11-28 10:20:50', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('101', '2', '全球合伙人', '2', 'flz_lawyer_ position', null, 'default', 'Y', '0', 'admin', '2019-11-28 10:21:15', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('102', '3', '合伙人律师', '3', 'flz_lawyer_ position', null, 'default', 'Y', '0', 'admin', '2019-11-28 10:21:34', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('103', '4', '执业律师', '4', 'flz_lawyer_ position', null, 'default', 'Y', '0', 'admin', '2019-11-28 10:21:45', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('104', '1', '最高院案例', '1', 'flz_case_type', null, 'default', 'Y', '0', 'admin', '2019-11-28 16:15:55', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('105', '2', '诉讼案例', '2', 'flz_case_type', null, 'default', 'Y', '0', 'admin', '2019-11-28 16:16:11', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('106', '3', '非诉案例', '3', 'flz_case_type', null, 'default', 'Y', '0', 'admin', '2019-11-28 16:16:21', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('107', '1', '展示', '1', 'flz_home_show', '', 'primary', 'Y', '0', 'admin', '2019-11-28 16:32:14', 'admin', '2019-11-28 16:36:39', '');
INSERT INTO `sys_dict_data` VALUES ('108', '1', '不展示', '0', 'flz_home_show', '', 'primary', 'Y', '0', 'admin', '2019-11-28 16:32:25', 'admin', '2019-11-28 16:36:35', '');
INSERT INTO `sys_dict_data` VALUES ('109', '1', '京师新闻', '1', 'flz_news_category', null, 'default', 'Y', '0', 'admin', '2019-11-28 17:24:34', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('110', '2', '京师党建', '2', 'flz_news_category', null, 'default', 'Y', '0', 'admin', '2019-11-28 17:24:45', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('111', '3', '京师公益', '3', 'flz_news_category', null, 'default', 'Y', '0', 'admin', '2019-11-28 17:24:55', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('112', '1', '行政诉讼', '1', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:25:54', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('113', '2', '劳动争议', '2', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:26:38', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('114', '3', '职务犯罪', '3', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:26:48', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('115', '4', '婚姻纠纷', '4', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:27:02', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('116', '5', '医疗纠纷', '5', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:27:19', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('117', '6', '工伤维权', '6', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:27:32', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('118', '7', '合同纠纷', '7', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:27:44', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('119', '8', '遗产继承', '8', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:28:00', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('120', '9', '公司法律', '9', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:28:13', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('121', '10', '知识产权', '10', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:28:31', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('122', '11', '房产建筑', '11', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:28:48', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('123', '12', '涉外纠纷', '12', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:28:58', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('124', '13', '其他', '13', 'flz_consult_caseType', null, 'default', 'Y', '0', 'admin', '2019-12-03 13:29:09', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('125', '1', '待联系', '0', 'flz_consult_status', null, 'danger', 'Y', '0', 'admin', '2019-12-03 13:33:08', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('126', '2', '已联系', '1', 'flz_consult_status', null, 'success', 'Y', '0', 'admin', '2019-12-03 13:33:24', '', null, null);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('100', '律师职位', 'flz_lawyer_ position', '0', 'admin', '2019-11-28 10:19:16', '', null, '法律站律师职位');
INSERT INTO `sys_dict_type` VALUES ('101', '案例类型', 'flz_case_type', '0', 'admin', '2019-11-28 16:15:31', '', null, '法律站案例类型');
INSERT INTO `sys_dict_type` VALUES ('102', '首页展示', 'flz_home_show', '0', 'admin', '2019-11-28 16:31:21', '', null, '用于首页案例展示');
INSERT INTO `sys_dict_type` VALUES ('103', '新闻分类', 'flz_news_category', '0', 'admin', '2019-11-28 17:24:04', '', null, '法律站新闻分类');
INSERT INTO `sys_dict_type` VALUES ('104', '案件类型', 'flz_consult_caseType', '0', 'admin', '2019-12-03 13:21:34', '', null, '法律站咨询管理案件类型');
INSERT INTO `sys_dict_type` VALUES ('105', '处理状态', 'flz_consult_status', '0', 'admin', '2019-12-03 13:32:40', '', null, '咨询管理处理状态');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('1', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-02 15:51:06');
INSERT INTO `sys_logininfor` VALUES ('2', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-02 17:53:56');
INSERT INTO `sys_logininfor` VALUES ('3', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-02 17:55:39');
INSERT INTO `sys_logininfor` VALUES ('4', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-02 17:57:36');
INSERT INTO `sys_logininfor` VALUES ('5', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-02 18:04:46');
INSERT INTO `sys_logininfor` VALUES ('6', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '1', '验证码错误', '2019-12-03 08:55:32');
INSERT INTO `sys_logininfor` VALUES ('7', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 08:55:35');
INSERT INTO `sys_logininfor` VALUES ('8', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 09:17:36');
INSERT INTO `sys_logininfor` VALUES ('9', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 09:20:43');
INSERT INTO `sys_logininfor` VALUES ('10', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 09:43:02');
INSERT INTO `sys_logininfor` VALUES ('11', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 10:09:32');
INSERT INTO `sys_logininfor` VALUES ('12', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 10:23:06');
INSERT INTO `sys_logininfor` VALUES ('13', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 13:10:00');
INSERT INTO `sys_logininfor` VALUES ('14', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '1', '验证码错误', '2019-12-03 13:43:36');
INSERT INTO `sys_logininfor` VALUES ('15', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 13:43:38');
INSERT INTO `sys_logininfor` VALUES ('16', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 13:48:20');
INSERT INTO `sys_logininfor` VALUES ('17', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 16:50:33');
INSERT INTO `sys_logininfor` VALUES ('18', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 17:00:45');
INSERT INTO `sys_logininfor` VALUES ('19', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 17:06:37');
INSERT INTO `sys_logininfor` VALUES ('20', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 17:14:25');
INSERT INTO `sys_logininfor` VALUES ('21', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-03 17:18:31');
INSERT INTO `sys_logininfor` VALUES ('22', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Mac OS X', '0', '登录成功', '2019-12-03 17:34:16');
INSERT INTO `sys_logininfor` VALUES ('23', 'admin', '124.202.229.62', 'XX XX', 'Chrome', 'Mac OS X', '0', '登录成功', '2019-12-03 18:16:09');
INSERT INTO `sys_logininfor` VALUES ('24', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Mac OS X', '0', '登录成功', '2019-12-04 09:13:00');
INSERT INTO `sys_logininfor` VALUES ('25', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-04 11:25:28');
INSERT INTO `sys_logininfor` VALUES ('26', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '退出成功', '2019-12-04 11:48:40');
INSERT INTO `sys_logininfor` VALUES ('27', 'admin', '124.202.229.62', '北京 北京', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-04 13:15:52');
INSERT INTO `sys_logininfor` VALUES ('28', 'admin', '124.202.229.62', 'XX XX', 'Chrome', 'Windows 7', '0', '退出成功', '2019-12-05 11:21:17');
INSERT INTO `sys_logininfor` VALUES ('29', 'admin', '124.202.229.62', 'XX XX', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-05 11:30:23');
INSERT INTO `sys_logininfor` VALUES ('30', 'admin', '124.202.229.62', 'XX XX', 'Chrome', 'Windows 7', '0', '登录成功', '2019-12-05 15:30:12');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2006 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', '#', '', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', '#', '', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '3', '#', '', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', '/system/user', '', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', '/system/notice', '', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '3', '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '表单构建', '3', '1', '/tool/build', '', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('114', '代码生成', '3', '2', '/tool/gen', '', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('115', '系统接口', '3', '3', '/tool/swagger', '', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', '6', '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', '7', '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', '1', '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', '2', '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', '3', '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', '4', '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', '5', '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单查询', '102', '1', '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单新增', '102', '2', '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单修改', '102', '3', '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单删除', '102', '4', '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', '1', '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', '2', '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', '3', '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', '4', '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '104', '1', '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '104', '2', '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '104', '3', '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '104', '4', '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '104', '5', '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1025', '字典查询', '105', '1', '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1026', '字典新增', '105', '2', '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1027', '字典修改', '105', '3', '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1028', '字典删除', '105', '4', '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1029', '字典导出', '105', '5', '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1030', '参数查询', '106', '1', '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1031', '参数新增', '106', '2', '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1032', '参数修改', '106', '3', '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1033', '参数删除', '106', '4', '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1034', '参数导出', '106', '5', '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '107', '1', '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '107', '2', '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '107', '3', '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '107', '4', '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', '1', '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', '2', '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1041', '详细信息', '500', '3', '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1046', '账户解锁', '501', '4', '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1047', '在线查询', '109', '1', '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1048', '批量强退', '109', '2', '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1049', '单条强退', '109', '3', '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1050', '任务查询', '110', '1', '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1051', '任务新增', '110', '2', '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1052', '任务修改', '110', '3', '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1053', '任务删除', '110', '4', '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1054', '状态修改', '110', '5', '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1055', '任务详细', '110', '6', '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1056', '任务导出', '110', '7', '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1057', '生成查询', '114', '1', '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1058', '生成修改', '114', '2', '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1059', '生成删除', '114', '3', '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1060', '预览代码', '114', '4', '#', '', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1061', '生成代码', '114', '5', '#', '', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('2000', '法律站管理', '0', '4', '#', 'menuItem', 'M', '0', null, 'fa fa-comments', 'admin', '2019-11-28 10:11:46', '', null, '');
INSERT INTO `sys_menu` VALUES ('2001', '律师团队', '2000', '2', '/system/resume', 'menuItem', 'C', '0', 'system:resume:view', 'fa fa-child', 'admin', '2019-11-28 10:12:50', '', null, '');
INSERT INTO `sys_menu` VALUES ('2002', '专业领域', '2000', '1', '/system/territory', 'menuItem', 'C', '0', 'system:territory:view', 'fa fa-anchor', 'admin', '2019-11-28 10:37:17', '', null, '');
INSERT INTO `sys_menu` VALUES ('2003', '案例管理', '2000', '3', '/system/case', 'menuItem', 'C', '0', 'system:case:view', 'fa fa-bell', 'admin', '2019-11-28 16:11:58', 'admin', '2019-11-28 16:12:34', '');
INSERT INTO `sys_menu` VALUES ('2004', '新闻资讯', '2000', '4', '/system/article', 'menuItem', 'C', '0', 'system:article:view', 'fa fa-book', 'admin', '2019-11-28 17:12:56', '', null, '');
INSERT INTO `sys_menu` VALUES ('2005', '咨询列表', '2000', '5', '/system/consult', 'menuItem', 'C', '0', 'system:consult:view', 'fa fa-balance-scale', 'admin', '2019-12-02 11:22:16', '', null, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1', '温馨提醒：2018-07-01 若依新版本发布啦', '2', '<p>新版本内容<img src=\"http://127.0.0.1:8087/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\" data-filename=\"/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\" style=\"width: 478px;\"></p>', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2019-12-02 16:49:55', '管理员');
INSERT INTO `sys_notice` VALUES ('2', '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('1', '操作日志', '9', 'com.ruoyi.web.controller.monitor.SysOperlogController.clean()', 'POST', '1', 'admin', '研发部门', '/monitor/operlog/clean', '127.0.0.1', '内网IP', '{ }', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 14:41:50');
INSERT INTO `sys_oper_log` VALUES ('2', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>qweqweqweqweqweqweq<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/be1827b4269db518f955a5d804f1181c.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/be1827b4269db518f955a5d804f1181c.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:39:10');
INSERT INTO `sys_oper_log` VALUES ('3', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>qweqweqweqweqweqweq<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/babc8cdbacdffcc997210b0f03bc8293.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/babc8cdbacdffcc997210b0f03bc8293.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:41:30');
INSERT INTO `sys_oper_log` VALUES ('4', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>胜多负少发<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/7bb3ee9a77443fc2e788398610d67e28.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/7bb3ee9a77443fc2e788398610d67e28.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:43:45');
INSERT INTO `sys_oper_log` VALUES ('5', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/96cf22fc5ff76bcf76a21e66ca4134d1.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/96cf22fc5ff76bcf76a21e66ca4134d1.jpg\\\" style=\\\"width: 455px;\\\">胜多负少发</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:46:16');
INSERT INTO `sys_oper_log` VALUES ('6', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"1\" ],\r\n  \"noticeTitle\" : [ \"温馨提醒：2018-07-01 若依新版本发布啦\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>新版本内容<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:47:38');
INSERT INTO `sys_oper_log` VALUES ('7', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>胜多负少发<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/b4e5842fc75e9083a5061ce1698aa3c8.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/b4e5842fc75e9083a5061ce1698aa3c8.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:51:20');
INSERT INTO `sys_oper_log` VALUES ('8', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>胜多负少发<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/678a5d5cc804688efd502eeb6da5bfd0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/678a5d5cc804688efd502eeb6da5bfd0.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:54:29');
INSERT INTO `sys_oper_log` VALUES ('9', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"articleTitle\" : [ \"请问\" ],\r\n  \"content\" : [ \"<p>请问<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/81dbd308a3806f606d8c5cb6daebf1ed.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/81dbd308a3806f606d8c5cb6daebf1ed.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"keywords\" : [ \"dddd\" ],\r\n  \"imageUrl\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 15:57:34');
INSERT INTO `sys_oper_log` VALUES ('10', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"1\" ],\r\n  \"noticeTitle\" : [ \"温馨提醒：2018-07-01 若依新版本发布啦\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>新版本内容<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:00:12');
INSERT INTO `sys_oper_log` VALUES ('11', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"articleDescribe\" : [ \"<p>去<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/94152b5df1fdf2a4e58c6a944aa5cd30.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/94152b5df1fdf2a4e58c6a944aa5cd30.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:02:34');
INSERT INTO `sys_oper_log` VALUES ('12', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>胜多负少发<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/c8887f05a2a1ed7d38083cd53ab74274.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/c8887f05a2a1ed7d38083cd53ab74274.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:03:37');
INSERT INTO `sys_oper_log` VALUES ('13', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"胜多负少发\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:08:07');
INSERT INTO `sys_oper_log` VALUES ('14', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>胜多负少发<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/bd473c60388698c45fc7ede739e6a4fb.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/bd473c60388698c45fc7ede739e6a4fb.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:08:56');
INSERT INTO `sys_oper_log` VALUES ('15', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"胜多负少发\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:19:18');
INSERT INTO `sys_oper_log` VALUES ('16', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<h1>as大蜀都中心</h1>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:19:36');
INSERT INTO `sys_oper_log` VALUES ('17', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/ee92a8f0452a57abbe4591aca0dac7e0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/ee92a8f0452a57abbe4591aca0dac7e0.jpg\\\" style=\\\"width: 451px;\\\">as大蜀都中心</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:24:37');
INSERT INTO `sys_oper_log` VALUES ('18', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/32e5ee4b9e866a78e72cd154ab13b930.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/32e5ee4b9e866a78e72cd154ab13b930.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:47:31');
INSERT INTO `sys_oper_log` VALUES ('19', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/notice/edit', '127.0.0.1', '内网IP', '{\r\n  \"noticeId\" : [ \"1\" ],\r\n  \"noticeTitle\" : [ \"温馨提醒：2018-07-01 若依新版本发布啦\" ],\r\n  \"noticeType\" : [ \"2\" ],\r\n  \"noticeContent\" : [ \"<p>新版本内容<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/ce47d00074ade8cf2087d834f13b59a0.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"status\" : [ \"0\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:49:55');
INSERT INTO `sys_oper_log` VALUES ('20', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+安安<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/1dfd7aecf88b712922e0d22eddb4e63c.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/1dfd7aecf88b712922e0d22eddb4e63c.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:50:29');
INSERT INTO `sys_oper_log` VALUES ('21', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/7552e9160ed2db01a3bffaec1b0a6a7f.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/7552e9160ed2db01a3bffaec1b0a6a7f.jpg\\\" style=\\\"width: 451px;\\\">as5656+56+安安</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:51:59');
INSERT INTO `sys_oper_log` VALUES ('22', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+安安<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/c9809970fd96233b0c5a2881668d9e2e.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/c9809970fd96233b0c5a2881668d9e2e.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:54:32');
INSERT INTO `sys_oper_log` VALUES ('23', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+安安<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/c4058fa73b8532917ea2ea10604e2bf7.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/c4058fa73b8532917ea2ea10604e2bf7.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:56:30');
INSERT INTO `sys_oper_log` VALUES ('24', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+安安<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/cee927512d27f432b6d62dc6a6fc6139.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/cee927512d27f432b6d62dc6a6fc6139.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 16:59:01');
INSERT INTO `sys_oper_log` VALUES ('25', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>as5656+56+安安<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/295cd231f4bc134a0fe7cf632e01e2d2.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/295cd231f4bc134a0fe7cf632e01e2d2.jpg\\\" style=\\\"width: 478px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:00:37');
INSERT INTO `sys_oper_log` VALUES ('26', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/0c71f640f7b19defe4f5683b7fbb2c66.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/0c71f640f7b19defe4f5683b7fbb2c66.jpg\\\" style=\\\"width: 478px;\\\">as5656+56+安安</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:17:39');
INSERT INTO `sys_oper_log` VALUES ('27', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'content\' doesn\'t have a default value\r\n### The error may involve com.ruoyi.system.mapper.flz.JsArticleMapper.insertJsArticle-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into js_article          ( article_title,                                       article_describe,             category_id,                          keywords,             image_url,                                                                                           create_time )           values ( ?,                                       ?,             ?,                          ?,             ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'content\' doesn\'t have a default value\n; Field \'content\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'content\' doesn\'t have a default value', '2019-12-02 17:32:54');
INSERT INTO `sys_oper_log` VALUES ('28', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<div style=\\\"text-align: center;\\\">车展上是</div><div style=\\\"text-align: center;\\\"><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/d45b8f5373ad114952e70a6512bf84ba.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/d45b8f5373ad114952e70a6512bf84ba.jpg\\\" style=\\\"width: 451px;\\\"><br></div>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:33:18');
INSERT INTO `sys_oper_log` VALUES ('29', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>阿萨达阿萨达<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/a42dd5dde17ac90e7891ec16d16f7a8c.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/a42dd5dde17ac90e7891ec16d16f7a8c.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:34:02');
INSERT INTO `sys_oper_log` VALUES ('30', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>手动阀吧<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/b740a73adb635c7acdaf407b964a969c.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/b740a73adb635c7acdaf407b964a969c.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:48:15');
INSERT INTO `sys_oper_log` VALUES ('31', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/25663b1416aae16ae83dbc26f5150415.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/25663b1416aae16ae83dbc26f5150415.jpg\\\" style=\\\"width: 455px;\\\">手动阀吧</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:52:09');
INSERT INTO `sys_oper_log` VALUES ('32', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>手动阀吧啊<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/74d4455b73738d7ad24d5988bdc60cb1.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/74d4455b73738d7ad24d5988bdc60cb1.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:53:01');
INSERT INTO `sys_oper_log` VALUES ('33', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/121ea3c07150acf14f59aa4ba79f4d5c.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/121ea3c07150acf14f59aa4ba79f4d5c.jpg\\\" style=\\\"width: 455px;\\\">手动阀吧啊</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:55:50');
INSERT INTO `sys_oper_log` VALUES ('34', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>阿萨达&nbsp; &nbsp;as&nbsp;<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/ee594d45f1530918a14335d64c4fc919.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/ee594d45f1530918a14335d64c4fc919.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 17:57:51');
INSERT INTO `sys_oper_log` VALUES ('35', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>阿萨达&nbsp; &nbsp;as&nbsp;<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/02/294f4f29cfce9f9283ec8640ff1c8bb6.jpg\\\" data-filename=\\\"/profile/upload/2019/12/02/294f4f29cfce9f9283ec8640ff1c8bb6.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-02 18:04:56');
INSERT INTO `sys_oper_log` VALUES ('36', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>阿萨达&nbsp; &nbsp;as&nbsp;<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/3c3a039d8d04a8638ed52b59ec7e21e0.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/3c3a039d8d04a8638ed52b59ec7e21e0.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 08:55:55');
INSERT INTO `sys_oper_log` VALUES ('37', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p>阿萨达&nbsp; &nbsp;as as</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 09:12:14');
INSERT INTO `sys_oper_log` VALUES ('38', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<u>阿萨达&nbsp; &nbsp;as as</u>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 09:12:56');
INSERT INTO `sys_oper_log` VALUES ('39', '案例', '2', 'com.ruoyi.web.controller.flz.FlCaseController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/case/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"caseTitle\" : [ \"大声道\" ],\r\n  \"categoryId\" : [ \"3\" ],\r\n  \"isShow\" : [ \"1\" ],\r\n  \"courtName\" : [ \"23说的\" ],\r\n  \"sentenceType\" : [ \"阿萨达多\" ],\r\n  \"targetAmount\" : [ \"正常\" ],\r\n  \"catDesc\" : [ \"<p>达瓦所多asd&nbsp;<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/f79c52c63f83cb9c13f502a34794d7ce.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/f79c52c63f83cb9c13f502a34794d7ce.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 09:18:02');
INSERT INTO `sys_oper_log` VALUES ('40', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" style=\\\"width: 455px;\\\">阿萨达&nbsp; &nbsp;as as</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/db8242a7-b2d9-49a5-8c09-632549115502.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 09:20:55');
INSERT INTO `sys_oper_log` VALUES ('41', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"articleTitle\" : [ \"aweawasd\" ],\r\n  \"content\" : [ \"<p>as&nbsp; a<img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/424e3067b968873cce077e3ac7d31f53.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/424e3067b968873cce077e3ac7d31f53.jpg\\\" style=\\\"width: 451px;\\\"></p>\" ],\r\n  \"articleDescribe\" : [ \"assdd\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"keywords\" : [ \"asd\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 09:49:45');
INSERT INTO `sys_oper_log` VALUES ('42', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"articleTitle\" : [ \"dasdasdd\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/468fde20b77c9fd0a2800d4592d8fe5f.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/468fde20b77c9fd0a2800d4592d8fe5f.jpg\\\" style=\\\"width: 478px;\\\"><br></p>\" ],\r\n  \"articleDescribe\" : [ \"add\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"keywords\" : [ \"aadd\" ],\r\n  \"imageUrl\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 10:11:05');
INSERT INTO `sys_oper_log` VALUES ('43', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"articleTitle\" : [ \"sdad\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/6d9396219e3e7eba0ce1b0c0d1667b62.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/6d9396219e3e7eba0ce1b0c0d1667b62.jpg\\\" style=\\\"width: 478px;\\\"><br></p>\" ],\r\n  \"articleDescribe\" : [ \"d\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"keywords\" : [ \"d\" ],\r\n  \"imageUrl\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 10:12:48');
INSERT INTO `sys_oper_log` VALUES ('44', '文章', '1', 'com.ruoyi.web.controller.flz.JsArticleController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/article/add', '127.0.0.1', '内网IP', '{\r\n  \"articleTitle\" : [ \"asddd\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/4830a1bf5efbc3bb9d7aa2e14eebbfad.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/4830a1bf5efbc3bb9d7aa2e14eebbfad.jpg\\\" style=\\\"width: 478px;\\\"><br></p>\" ],\r\n  \"articleDescribe\" : [ \"as\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"keywords\" : [ \"d\" ],\r\n  \"imageUrl\" : [ \"/files/2afe6120-d81a-4f5b-8e2e-cbb25bc82d7c.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 10:16:40');
INSERT INTO `sys_oper_log` VALUES ('45', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '127.0.0.1', '内网IP', '{\r\n  \"id\" : [ \"1\" ],\r\n  \"articleTitle\" : [ \"qwe\" ],\r\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" style=\\\"width: 455px;\\\">阿萨达&nbsp; &nbsp;as as</p>\" ],\r\n  \"articleDescribe\" : [ \"去\" ],\r\n  \"categoryId\" : [ \"2\" ],\r\n  \"keywords\" : [ \"ssss\" ],\r\n  \"imageUrl\" : [ \"/files/4bfff625-6fc1-40d4-b099-51900cb0ebb9.jpg\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 11:11:53');
INSERT INTO `sys_oper_log` VALUES ('46', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"案件类型\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"法律站咨询管理案件类型\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:21:34');
INSERT INTO `sys_oper_log` VALUES ('47', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"行政诉讼\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:25:55');
INSERT INTO `sys_oper_log` VALUES ('48', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"劳动争议\" ],\r\n  \"dictValue\" : [ \"2\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:26:38');
INSERT INTO `sys_oper_log` VALUES ('49', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"职务犯罪\" ],\r\n  \"dictValue\" : [ \"3\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"3\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:26:48');
INSERT INTO `sys_oper_log` VALUES ('50', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"婚姻纠纷\" ],\r\n  \"dictValue\" : [ \"4\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"4\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:27:02');
INSERT INTO `sys_oper_log` VALUES ('51', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"医疗纠纷\" ],\r\n  \"dictValue\" : [ \"5\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"5\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:27:19');
INSERT INTO `sys_oper_log` VALUES ('52', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"工伤维权\" ],\r\n  \"dictValue\" : [ \"6\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"6\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:27:32');
INSERT INTO `sys_oper_log` VALUES ('53', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"合同纠纷\" ],\r\n  \"dictValue\" : [ \"7\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"7\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:27:44');
INSERT INTO `sys_oper_log` VALUES ('54', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"遗产继承\" ],\r\n  \"dictValue\" : [ \"8\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"8\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:28:00');
INSERT INTO `sys_oper_log` VALUES ('55', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"公司法律\" ],\r\n  \"dictValue\" : [ \"9\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"9\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:28:13');
INSERT INTO `sys_oper_log` VALUES ('56', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"知识产权\" ],\r\n  \"dictValue\" : [ \"10\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"10\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:28:31');
INSERT INTO `sys_oper_log` VALUES ('57', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"房产建筑\" ],\r\n  \"dictValue\" : [ \"11\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"11\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:28:48');
INSERT INTO `sys_oper_log` VALUES ('58', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"涉外纠纷\" ],\r\n  \"dictValue\" : [ \"12\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"12\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:28:58');
INSERT INTO `sys_oper_log` VALUES ('59', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"其他\" ],\r\n  \"dictValue\" : [ \"13\" ],\r\n  \"dictType\" : [ \"flz_consult_caseType\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"13\" ],\r\n  \"listClass\" : [ \"default\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:29:09');
INSERT INTO `sys_oper_log` VALUES ('60', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\r\n  \"dictName\" : [ \"处理状态\" ],\r\n  \"dictType\" : [ \"flz_consult_status\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"咨询管理处理状态\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:32:40');
INSERT INTO `sys_oper_log` VALUES ('61', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"待联系\" ],\r\n  \"dictValue\" : [ \"0\" ],\r\n  \"dictType\" : [ \"flz_consult_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"1\" ],\r\n  \"listClass\" : [ \"danger\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:33:08');
INSERT INTO `sys_oper_log` VALUES ('62', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\r\n  \"dictLabel\" : [ \"已联系\" ],\r\n  \"dictValue\" : [ \"1\" ],\r\n  \"dictType\" : [ \"flz_consult_status\" ],\r\n  \"cssClass\" : [ \"\" ],\r\n  \"dictSort\" : [ \"2\" ],\r\n  \"listClass\" : [ \"success\" ],\r\n  \"isDefault\" : [ \"Y\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 13:33:24');
INSERT INTO `sys_oper_log` VALUES ('63', '案例', '1', 'com.ruoyi.web.controller.flz.FlCaseController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/case/add', '127.0.0.1', '内网IP', '{\r\n  \"caseTitle\" : [ \"葫芦熊德\" ],\r\n  \"categoryId\" : [ \"1\" ],\r\n  \"isShow\" : [ \"0\" ],\r\n  \"courtName\" : [ \"\" ],\r\n  \"sentenceType\" : [ \"\" ],\r\n  \"targetAmount\" : [ \"\" ],\r\n  \"catDesc\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/78f471e0931a900723b7301753693584.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/78f471e0931a900723b7301753693584.jpg\\\" style=\\\"width: 455px;\\\">阿萨达<br></p>\" ]\r\n}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2019-12-03 16:08:09');
INSERT INTO `sys_oper_log` VALUES ('64', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', 'POST', '1', 'admin', '研发部门', '/system/user/profile/updateAvatar', '124.202.229.62', 'XX XX', '{ }', '{\n  \"msg\" : \"java.io.FileNotFoundException: /tmp/tomcat.7400684379949616393.8087/work/Tomcat/localhost/ROOT/D:/ruoyi/uploadPath/avatar/2019/12/03/83ad2184352478bac3c73ef0c1752ae8.png (No such file or directory)\",\n  \"code\" : 500\n}', '0', null, '2019-12-03 17:01:14');
INSERT INTO `sys_oper_log` VALUES ('65', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', 'POST', '1', 'admin', '研发部门', '/system/user/profile/updateAvatar', '124.202.229.62', '北京 北京', '{ }', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:06:48');
INSERT INTO `sys_oper_log` VALUES ('66', '领域', '1', 'com.ruoyi.web.controller.flz.FlTerritoryController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/territory/add', '124.202.229.62', '北京 北京', '{\n  \"parentId\" : [ \"2\" ],\n  \"territoryName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"catDesc\" : [ \"\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:08:30');
INSERT INTO `sys_oper_log` VALUES ('67', '领域', '3', 'com.ruoyi.web.controller.flz.FlTerritoryController.remove()', 'GET', '1', 'admin', '研发部门', '/system/territory/remove/7', '124.202.229.62', '北京 北京', '{ }', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:08:40');
INSERT INTO `sys_oper_log` VALUES ('68', '领域', '2', 'com.ruoyi.web.controller.flz.FlTerritoryController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/territory/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"2\" ],\n  \"parentId\" : [ \"1\" ],\n  \"parentName\" : [ \"光\" ],\n  \"territoryName\" : [ \"大娃\" ],\n  \"imageUrl\" : [ \"/files/1d99e31c-12d7-4c72-98f0-8e04d7e09372.jpg\" ],\n  \"catDesc\" : [ \"葫芦娃老大\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:08:49');
INSERT INTO `sys_oper_log` VALUES ('69', '领域', '2', 'com.ruoyi.web.controller.flz.FlTerritoryController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/territory/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"parentId\" : [ \"0\" ],\n  \"parentName\" : [ \"\" ],\n  \"territoryName\" : [ \"光\" ],\n  \"imageUrl\" : [ \"/files/5e63fe77-42a1-4ae5-8cf6-83357f7b693d.jpg\" ],\n  \"catDesc\" : [ \"爷爷\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:14:37');
INSERT INTO `sys_oper_log` VALUES ('70', '案例', '2', 'com.ruoyi.web.controller.flz.FlCaseController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/case/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"caseTitle\" : [ \"大声道\" ],\n  \"categoryId\" : [ \"3\" ],\n  \"isShow\" : [ \"1\" ],\n  \"courtName\" : [ \"23说的\" ],\n  \"sentenceType\" : [ \"阿萨达多\" ],\n  \"targetAmount\" : [ \"正常\" ],\n  \"catDesc\" : [ \"达瓦所多asd&nbsp;\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:23:05');
INSERT INTO `sys_oper_log` VALUES ('71', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"2\" ],\n  \"name\" : [ \"大娃\" ],\n  \"englishName\" : [ \"dawa\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"2\" ],\n  \"address\" : [ \"北京\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">香港最近不太平。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">一些激进势力以“反修例”为幌子进行各种激进抗争活动，暴力化程度不断升级。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">《人民日报》称：香港正面临回归以来最严峻的局面。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">反对派激进示威者不但袭警、打砸、阻断交通，甚至污损国徽，踩踏国旗。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">前几天，他们先后两次，从香港尖沙咀五支旗杆拆下国旗，扔入大海。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\"><img width=\\\"550\\\" src=\\\"http://www.jingdianwenzhang.cn/img.php?url=https://mmbiz.qpic.cn/mmbiz_jpg/icB0yCLh6LJvsdDGbicKd3fSfcRrAJjzZnZO7e9qV5viatAtgJC52y0qjeBCDOS8nwGpZwiacldaJ5QC5myjiax93JQ/640?wx_fmt=jpeg\\\" data-backw=\\\"356\\\" data-backh=\\\"331\\\" data-before-oversubscription-url=\\\"https://mmbiz.qpic.cn/mmbiz_jpg/icB0yCLh6LJvsdDGbicKd3fSfcRrAJjzZnZO7e9qV5viatAtgJC52y0qjeBCDOS8nwGpZwiacldaJ5QC5myjiax93JQ/640?wx_fmt=jpeg\\\" data-bd-imgshare-binded=\\\"1\\\" style=\\\"margin: 0px; padding: 0px;\\\"><br></p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">这一幕，实在令人痛心。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 5', 'null', '1', '', '2019-12-03 17:24:51');
INSERT INTO `sys_oper_log` VALUES ('72', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"name\" : [ \"光\" ],\n  \"englishName\" : [ \"guang\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"1\" ],\n  \"territoryIds\" : [ \"2\", \"3\" ],\n  \"address\" : [ \"北京\" ],\n  \"recommend\" : [ \"<p><img src=\\\"http://39.106.213.243:8087/profile/upload/2019/12/03/7bc8fbc025486622d1cf194d723ab2e3.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/7bc8fbc025486622d1cf194d723ab2e3.jpg\\\" style=\\\"width: 451px;\\\">asdasd</p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:25:42');
INSERT INTO `sys_oper_log` VALUES ('73', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"name\" : [ \"光\" ],\n  \"englishName\" : [ \"guang\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"1\" ],\n  \"territoryIds\" : [ \"2\", \"3\" ],\n  \"address\" : [ \"北京\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">香港最近不太平。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">一些激进势力以“反修例”为幌子进行各种激进抗争活动，暴力化程度不断升级。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">《人民日报》称：香港正面临回归以来最严峻的局面。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">反对派激进示威者不但袭警、打砸、阻断交通，甚至污损国徽，踩踏国旗。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">前几天，他们先后两次，从香港尖沙咀五支旗杆拆下国旗，扔入大海。</p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\"><img width=\\\"550\\\" src=\\\"http://www.jingdianwenzhang.cn/img.php?url=https://mmbiz.qpic.cn/mmbiz_jpg/icB0yCLh6LJvsdDGbicKd3fSfcRrAJjzZnZO7e9qV5viatAtgJC52y0qjeBCDOS8nwGpZwiacldaJ5QC5myjiax93JQ/640?wx_fmt=jpeg\\\" data-backw=\\\"356\\\" data-backh=\\\"331\\\" data-before-oversubscription-url=\\\"https://mmbiz.qpic.cn/mmbiz_jpg/icB0yCLh6LJvsdDGbicKd3fSfcRrAJjzZnZO7e9qV5viatAtgJC52y0qjeBCDOS8nwGpZwiacldaJ5QC5myjiax93JQ/640?wx_fmt=jpeg\\\" data-bd-imgshare-binded=\\\"1\\\" style=\\\"margin: 0px; padding: 0px;\\\"><br></p><p style=\\\"margin-bottom: 15px; padding: 0px; text-indent: 2em; color: rgb(51, 51, 51); font-family: &quot;microsoft yahei&quot;, arial; font-size: 14px;\\\">这一幕，实在令人痛心。</p><p style=\\\"margin-bottom: 15px; padding: 0px; t', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-03 17:25:54');
INSERT INTO `sys_oper_log` VALUES ('74', '个人简介', '3', 'com.ruoyi.web.controller.flz.JsResumeController.remove()', 'POST', '1', 'admin', '研发部门', '/system/resume/remove', '124.202.229.62', '北京 北京', '{\n  \"ids\" : [ \"1,2,3,4,5,6\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:20:44');
INSERT INTO `sys_oper_log` VALUES ('75', '个人简介', '1', 'com.ruoyi.web.controller.flz.JsResumeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/add', '124.202.229.62', '北京 北京', '{\n  \"name\" : [ \"卜传武\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"2\", \"5\" ],\n  \"address\" : [ \"昆明\" ],\n  \"recommend\" : [ \"<p style=\\\"text-align: justify; \\\"><span style=\\\"color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">&nbsp; &nbsp; &nbsp; 清华大学法学硕士（民商法学专业），2008年8月起任北京市盈科律师事务所合伙人。2010年创建北京盈科（昆明）律所事务所，并任首任主任。2018年4月创立北京京师（昆明）律师事务所。中国法学会会员、中国法学会民商法学研究会理事、云南省青联常委、昆明市青联常委、昆明市企业联合团委副书记、云南省志愿者协会总法律顾问。</span><br></p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:23:41');
INSERT INTO `sys_oper_log` VALUES ('76', '个人简介', '1', 'com.ruoyi.web.controller.flz.JsResumeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/add', '124.202.229.62', '北京 北京', '{\n  \"name\" : [ \"曹发平\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"1\", \"6\" ],\n  \"address\" : [ \"上海\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><b>教育背景：</b></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">2013.09—2016.06&nbsp;&nbsp; 复旦大学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 专业：国际经济法&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 学位：法学硕士<br>2009.09—2013.06&nbsp;&nbsp; 山东理工大学&nbsp;&nbsp; 专业：法学&nbsp;/&nbsp;国际经济与贸易&nbsp;&nbsp; 学位：法学学士&nbsp;/&nbsp;经济学学士</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">工作经历：</span><br>l 全程参与律师团队为某企业在“新三板”转让系统挂牌项目，参与尽调，并出具法律意见书；<br>l 全程参与律师团队私募基金管理人登记和私募基金的产品备案项目，并出具法律意见书<br>l 负责律师团队为某大型企业股权收购案件的全程法律尽职调查及起草、审核尽调报告；<br>l 负责律师团队的私募基金业务研究工作，负责为团队金融、基金类客户提供法律咨询服务。</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">学术研究：</span><br>1. 2015.07 在期刊《南京师范大学报》上发表文章《大陆架划界规则的新发展及其评述》<br>2. 2015.04 与同学共同承担“扩大《关于货物暂准进口的&nbsp;ATA&nbsp;报关单证册海关公约》适用范围”课题<br>3. 2013.11 承担复旦大学上海自贸区法研究中心期刊《上海自贸区法研究与动态》（月刊）的编务工作<br>4. 2013.10 获得复旦大学法学院&nbsp;2013&nbsp;级法学硕士研究生第三等新生学业奖学金<br>5. 2013.07 在期刊《法制与经济》上发表文章《试述法国总统制度及总统选举制度》<br>6. 2013.06 本科毕业论文《专利标准化法律问题初探》被评为山东理工大学校级优秀毕业论文</p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:26:28');
INSERT INTO `sys_oper_log` VALUES ('77', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"8\" ],\n  \"name\" : [ \"曹发平\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"1\", \"6\" ],\n  \"address\" : [ \"上海\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><b>教育背景：</b></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">2013.09—2016.06&nbsp;&nbsp; 复旦大学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 专业：国际经济法&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 学位：法学硕士<br>2009.09—2013.06&nbsp;&nbsp; 山东理工大学&nbsp;&nbsp; 专业：法学&nbsp;/&nbsp;国际经济与贸易&nbsp;&nbsp; 学位：法学学士&nbsp;/&nbsp;经济学学士</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">工作经历：</span><br>l 全程参与律师团队为某企业在“新三板”转让系统挂牌项目，参与尽调，并出具法律意见书；<br>l 全程参与律师团队私募基金管理人登记和私募基金的产品备案项目，并出具法律意见书<br>l 负责律师团队为某大型企业股权收购案件的全程法律尽职调查及起草、审核尽调报告；<br>l 负责律师团队的私募基金业务研究工作，负责为团队金融、基金类客户提供法律咨询服务。</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">学术研究：</span><br>1. 2015.07 在期刊《南京师范大学报》上发表文章《大陆架划界规则的新发展及其评述》<br>2. 2015.04 与同学共同承担“扩大《关于货物暂准进口的&nbsp;ATA&nbsp;报关单证册海关公约》适用范围”课题<br>3. 2013.11 承担复旦大学上海自贸区法研究中心期刊《上海自贸区法研究与动态》（月刊）的编务工作<br>4. 2013.10 获得复旦大学法学院&nbsp;2013&nbsp;级法学硕士研究生第三等新生学业奖学金<br>5. 2013.07 在期刊《法制与经济》上发表文章《试述法国总统制度及总统选举制度》<br>6. 2013.06 本科毕业论文《专利标准化法律问题初探》被评为山东理工大学校级优秀毕业论文</p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:44:46');
INSERT INTO `sys_oper_log` VALUES ('78', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"7\" ],\n  \"name\" : [ \"卜传武\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"2\", \"5\" ],\n  \"address\" : [ \"昆明\" ],\n  \"recommend\" : [ \"<p style=\\\"text-align: justify; \\\"><span style=\\\"color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">&nbsp; &nbsp; &nbsp; 清华大学法学硕士（民商法学专业），2008年8月起任北京市盈科律师事务所合伙人。2010年创建北京盈科（昆明）律所事务所，并任首任主任。2018年4月创立北京京师（昆明）律师事务所。中国法学会会员、中国法学会民商法学研究会理事、云南省青联常委、昆明市青联常委、昆明市企业联合团委副书记、云南省志愿者协会总法律顾问。</span><br></p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:44:57');
INSERT INTO `sys_oper_log` VALUES ('79', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"8\" ],\n  \"name\" : [ \"曹发平\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"1\", \"6\" ],\n  \"address\" : [ \"上海\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><b>教育背景：</b></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">2013.09—2016.06&nbsp;&nbsp; 复旦大学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 专业：国际经济法&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 学位：法学硕士<br>2009.09—2013.06&nbsp;&nbsp; 山东理工大学&nbsp;&nbsp; 专业：法学&nbsp;/&nbsp;国际经济与贸易&nbsp;&nbsp; 学位：法学学士&nbsp;/&nbsp;经济学学士</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">工作经历：</span><br>l 全程参与律师团队为某企业在“新三板”转让系统挂牌项目，参与尽调，并出具法律意见书；<br>l 全程参与律师团队私募基金管理人登记和私募基金的产品备案项目，并出具法律意见书<br>l 负责律师团队为某大型企业股权收购案件的全程法律尽职调查及起草、审核尽调报告；<br>l 负责律师团队的私募基金业务研究工作，负责为团队金融、基金类客户提供法律咨询服务。</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">学术研究：</span><br>1. 2015.07 在期刊《南京师范大学报》上发表文章《大陆架划界规则的新发展及其评述》<br>2. 2015.04 与同学共同承担“扩大《关于货物暂准进口的&nbsp;ATA&nbsp;报关单证册海关公约》适用范围”课题<br>3. 2013.11 承担复旦大学上海自贸区法研究中心期刊《上海自贸区法研究与动态》（月刊）的编务工作<br>4. 2013.10 获得复旦大学法学院&nbsp;2013&nbsp;级法学硕士研究生第三等新生学业奖学金<br>5. 2013.07 在期刊《法制与经济》上发表文章《试述法国总统制度及总统选举制度》<br>6. 2013.06 本科毕业论文《专利标准化法律问题初探》被评为山东理工大学校级优秀毕业论文</p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:45:06');
INSERT INTO `sys_oper_log` VALUES ('80', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"8\" ],\n  \"name\" : [ \"曹发平\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"/files/ab38f8da-8aea-4b2e-9528-524caf011980.jpg\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"1\", \"6\" ],\n  \"address\" : [ \"上海\" ],\n  \"recommend\" : [ \"<p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><b>教育背景：</b></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">2013.09—2016.06&nbsp;&nbsp; 复旦大学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 专业：国际经济法&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 学位：法学硕士<br>2009.09—2013.06&nbsp;&nbsp; 山东理工大学&nbsp;&nbsp; 专业：法学&nbsp;/&nbsp;国际经济与贸易&nbsp;&nbsp; 学位：法学学士&nbsp;/&nbsp;经济学学士</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">工作经历：</span><br>l 全程参与律师团队为某企业在“新三板”转让系统挂牌项目，参与尽调，并出具法律意见书；<br>l 全程参与律师团队私募基金管理人登记和私募基金的产品备案项目，并出具法律意见书<br>l 负责律师团队为某大型企业股权收购案件的全程法律尽职调查及起草、审核尽调报告；<br>l 负责律师团队的私募基金业务研究工作，负责为团队金融、基金类客户提供法律咨询服务。</p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><br></p><p style=\\\"margin-top: 15px; color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\"><span style=\\\"font-weight: 700;\\\">学术研究：</span><br>1. 2015.07 在期刊《南京师范大学报》上发表文章《大陆架划界规则的新发展及其评述》<br>2. 2015.04 与同学共同承担“扩大《关于货物暂准进口的&nbsp;ATA&nbsp;报关单证册海关公约》适用范围”课题<br>3. 2013.11 承担复旦大学上海自贸区法研究中心期刊《上海自贸区法研究与动态》（月刊）的编务工作<br>4. 2013.10 获得复旦大学法学院&nbsp;2013&nbsp;级法学硕士研究生第三等新生学业奖学金<br>5. 2013.07 在期刊《法制与经济》上发表文章《试述法国总统制度及总统选举制度》<br>6. 2013.06 本科毕业论文《专利标准化法律问题初探》被评为山东理工大学校级优秀毕业论文</p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:45:19');
INSERT INTO `sys_oper_log` VALUES ('81', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"7\" ],\n  \"name\" : [ \"卜传武\" ],\n  \"englishName\" : [ \"\" ],\n  \"imageUrl\" : [ \"/files/fc5a55c6-af55-4118-9994-0a7add82e1f9.jpg\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"2\", \"5\" ],\n  \"address\" : [ \"昆明\" ],\n  \"recommend\" : [ \"<p style=\\\"text-align: justify; \\\"><span style=\\\"color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">&nbsp; &nbsp; &nbsp; 清华大学法学硕士（民商法学专业），2008年8月起任北京市盈科律师事务所合伙人。2010年创建北京盈科（昆明）律所事务所，并任首任主任。2018年4月创立北京京师（昆明）律师事务所。中国法学会会员、中国法学会民商法学研究会理事、云南省青联常委、昆明市青联常委、昆明市企业联合团委副书记、云南省志愿者协会总法律顾问。</span><br></p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 09:45:29');
INSERT INTO `sys_oper_log` VALUES ('82', '个人简介', '2', 'com.ruoyi.web.controller.flz.JsResumeController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/resume/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"name\" : [ \"卜传武\" ],\n  \"englishName\" : [ \"w\" ],\n  \"imageUrl\" : [ \"/files/fc5a55c6-af55-4118-9994-0a7add82e1f9.jpg\" ],\n  \"email\" : [ \"\" ],\n  \"position\" : [ \"4\" ],\n  \"territoryIds\" : [ \"1\", \"2\", \"3\", \"5\", \"6\" ],\n  \"address\" : [ \"昆明\" ],\n  \"recommend\" : [ \"<p style=\\\"text-align: justify; \\\"><span style=\\\"color: rgb(51, 51, 51); font-family: 微软雅黑, 宋体; font-size: 15.6px;\\\">&nbsp; &nbsp; &nbsp; 清华大学法学硕士（民商法学专业），2008年8月起任北京市盈科律师事务所合伙人。2010年创建北京盈科（昆明）律所事务所，并任首任主任。2018年4月创立北京京师（昆明）律师事务所。中国法学会会员、中国法学会民商法学研究会理事、云南省青联常委、昆明市青联常委、昆明市企业联合团委副书记、云南省志愿者协会总法律顾问。</span><br></p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-04 11:27:04');
INSERT INTO `sys_oper_log` VALUES ('83', '案例', '1', 'com.ruoyi.web.controller.flz.FlCaseController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/case/add', '124.202.229.62', '北京 北京', '{\n  \"caseTitle\" : [ \"这是一个案例\" ],\n  \"categoryId\" : [ \"1\" ],\n  \"isShow\" : [ \"1\" ],\n  \"courtName\" : [ \"人民\" ],\n  \"sentenceType\" : [ \"额  不知道\" ],\n  \"targetAmount\" : [ \"1100\" ],\n  \"caseDepict\" : [ \"案例就是案例\" ],\n  \"catDesc\" : [ \"<p>啊但是大说的<img src=\\\"http://39.106.213.243:8087/profile/upload/2019/12/05/d3813dce3382b2b09679ad74c4d34a74.jpg\\\" data-filename=\\\"/profile/upload/2019/12/05/d3813dce3382b2b09679ad74c4d34a74.jpg\\\" style=\\\"width: 455px;\\\"></p>\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-05 10:44:29');
INSERT INTO `sys_oper_log` VALUES ('84', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '124.202.229.62', '北京 北京', '{\n  \"id\" : [ \"1\" ],\n  \"articleTitle\" : [ \"qwe\" ],\n  \"imageUrl\" : [ \"/files/cc985ed1-034f-4588-8735-c1c2b6d89c29.jpg\" ],\n  \"content\" : [ \"<p><img src=\\\"http://127.0.0.1:8087/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" data-filename=\\\"/profile/upload/2019/12/03/c3fed0271bfd59828281b890dc98d17f.jpg\\\" style=\\\"width: 455px;\\\">阿萨达&nbsp; &nbsp;as as</p>\" ],\n  \"articleDescribe\" : [ \"去\" ],\n  \"categoryId\" : [ \"2\" ],\n  \"keywords\" : [ \"ssss\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-05 11:30:57');
INSERT INTO `sys_oper_log` VALUES ('85', '文章', '2', 'com.ruoyi.web.controller.flz.JsArticleController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/article/edit', '124.202.229.62', 'XX XX', '{\n  \"id\" : [ \"1\" ],\n  \"articleTitle\" : [ \"qwe\" ],\n  \"imageUrl\" : [ \"/files/cc985ed1-034f-4588-8735-c1c2b6d89c29.jpg\" ],\n  \"content\" : [ \"<p><img src=\\\"http://39.106.213.243:8087/profile/upload/2019/12/05/254d0ad771fb58f28de4f867da75ec23.jpg\\\" data-filename=\\\"/profile/upload/2019/12/05/254d0ad771fb58f28de4f867da75ec23.jpg\\\" style=\\\"width: 451px;\\\">阿萨达&nbsp; &nbsp;as as</p>\" ],\n  \"articleDescribe\" : [ \"去\" ],\n  \"categoryId\" : [ \"2\" ],\n  \"keywords\" : [ \"ssss\" ]\n}', '{\n  \"msg\" : \"操作成功\",\n  \"code\" : 0\n}', '0', null, '2019-12-05 11:32:32');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '董事长', '1', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES ('2', 'se', '项目经理', '2', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES ('3', 'hr', '人力资源', '3', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_post` VALUES ('4', 'user', '普通员工', '4', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', 'admin', '1', '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('2', '100');
INSERT INTO `sys_role_dept` VALUES ('2', '101');
INSERT INTO `sys_role_dept` VALUES ('2', '105');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '105');
INSERT INTO `sys_role_menu` VALUES ('2', '106');
INSERT INTO `sys_role_menu` VALUES ('2', '107');
INSERT INTO `sys_role_menu` VALUES ('2', '108');
INSERT INTO `sys_role_menu` VALUES ('2', '109');
INSERT INTO `sys_role_menu` VALUES ('2', '110');
INSERT INTO `sys_role_menu` VALUES ('2', '111');
INSERT INTO `sys_role_menu` VALUES ('2', '112');
INSERT INTO `sys_role_menu` VALUES ('2', '113');
INSERT INTO `sys_role_menu` VALUES ('2', '114');
INSERT INTO `sys_role_menu` VALUES ('2', '115');
INSERT INTO `sys_role_menu` VALUES ('2', '500');
INSERT INTO `sys_role_menu` VALUES ('2', '501');
INSERT INTO `sys_role_menu` VALUES ('2', '1000');
INSERT INTO `sys_role_menu` VALUES ('2', '1001');
INSERT INTO `sys_role_menu` VALUES ('2', '1002');
INSERT INTO `sys_role_menu` VALUES ('2', '1003');
INSERT INTO `sys_role_menu` VALUES ('2', '1004');
INSERT INTO `sys_role_menu` VALUES ('2', '1005');
INSERT INTO `sys_role_menu` VALUES ('2', '1006');
INSERT INTO `sys_role_menu` VALUES ('2', '1007');
INSERT INTO `sys_role_menu` VALUES ('2', '1008');
INSERT INTO `sys_role_menu` VALUES ('2', '1009');
INSERT INTO `sys_role_menu` VALUES ('2', '1010');
INSERT INTO `sys_role_menu` VALUES ('2', '1011');
INSERT INTO `sys_role_menu` VALUES ('2', '1012');
INSERT INTO `sys_role_menu` VALUES ('2', '1013');
INSERT INTO `sys_role_menu` VALUES ('2', '1014');
INSERT INTO `sys_role_menu` VALUES ('2', '1015');
INSERT INTO `sys_role_menu` VALUES ('2', '1016');
INSERT INTO `sys_role_menu` VALUES ('2', '1017');
INSERT INTO `sys_role_menu` VALUES ('2', '1018');
INSERT INTO `sys_role_menu` VALUES ('2', '1019');
INSERT INTO `sys_role_menu` VALUES ('2', '1020');
INSERT INTO `sys_role_menu` VALUES ('2', '1021');
INSERT INTO `sys_role_menu` VALUES ('2', '1022');
INSERT INTO `sys_role_menu` VALUES ('2', '1023');
INSERT INTO `sys_role_menu` VALUES ('2', '1024');
INSERT INTO `sys_role_menu` VALUES ('2', '1025');
INSERT INTO `sys_role_menu` VALUES ('2', '1026');
INSERT INTO `sys_role_menu` VALUES ('2', '1027');
INSERT INTO `sys_role_menu` VALUES ('2', '1028');
INSERT INTO `sys_role_menu` VALUES ('2', '1029');
INSERT INTO `sys_role_menu` VALUES ('2', '1030');
INSERT INTO `sys_role_menu` VALUES ('2', '1031');
INSERT INTO `sys_role_menu` VALUES ('2', '1032');
INSERT INTO `sys_role_menu` VALUES ('2', '1033');
INSERT INTO `sys_role_menu` VALUES ('2', '1034');
INSERT INTO `sys_role_menu` VALUES ('2', '1035');
INSERT INTO `sys_role_menu` VALUES ('2', '1036');
INSERT INTO `sys_role_menu` VALUES ('2', '1037');
INSERT INTO `sys_role_menu` VALUES ('2', '1038');
INSERT INTO `sys_role_menu` VALUES ('2', '1039');
INSERT INTO `sys_role_menu` VALUES ('2', '1040');
INSERT INTO `sys_role_menu` VALUES ('2', '1041');
INSERT INTO `sys_role_menu` VALUES ('2', '1042');
INSERT INTO `sys_role_menu` VALUES ('2', '1043');
INSERT INTO `sys_role_menu` VALUES ('2', '1044');
INSERT INTO `sys_role_menu` VALUES ('2', '1045');
INSERT INTO `sys_role_menu` VALUES ('2', '1046');
INSERT INTO `sys_role_menu` VALUES ('2', '1047');
INSERT INTO `sys_role_menu` VALUES ('2', '1048');
INSERT INTO `sys_role_menu` VALUES ('2', '1049');
INSERT INTO `sys_role_menu` VALUES ('2', '1050');
INSERT INTO `sys_role_menu` VALUES ('2', '1051');
INSERT INTO `sys_role_menu` VALUES ('2', '1052');
INSERT INTO `sys_role_menu` VALUES ('2', '1053');
INSERT INTO `sys_role_menu` VALUES ('2', '1054');
INSERT INTO `sys_role_menu` VALUES ('2', '1055');
INSERT INTO `sys_role_menu` VALUES ('2', '1056');
INSERT INTO `sys_role_menu` VALUES ('2', '1057');
INSERT INTO `sys_role_menu` VALUES ('2', '1058');
INSERT INTO `sys_role_menu` VALUES ('2', '1059');
INSERT INTO `sys_role_menu` VALUES ('2', '1060');
INSERT INTO `sys_role_menu` VALUES ('2', '1061');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '/profile/avatar/2019/12/03/8d7a78da4269adc75af65ad27b5fd302.png', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '124.202.229.62', '2019-12-05 15:30:12', 'admin', '2018-03-16 11:33:00', 'ry', '2019-12-05 15:30:11', '管理员');
INSERT INTO `sys_user` VALUES ('2', '105', 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '测试员');

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('d57e369c-d4d7-477e-964d-2c463a18b54c', 'admin', '研发部门', '124.202.229.62', 'XX XX', 'Chrome', 'Windows 7', 'on_line', '2019-12-05 15:30:05', '2019-12-05 15:30:12', '1800000');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');
INSERT INTO `sys_user_post` VALUES ('2', '2');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');

-- ----------------------------
-- Table structure for territory_resume
-- ----------------------------
DROP TABLE IF EXISTS `territory_resume`;
CREATE TABLE `territory_resume` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `territory_id` bigint(20) DEFAULT '0' COMMENT '领域id',
  `resume_id` bigint(20) DEFAULT '0' COMMENT '律师id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='律师与领域关联表';

-- ----------------------------
-- Records of territory_resume
-- ----------------------------
